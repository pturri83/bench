%% PARAMETERS FOR REVOLT SIMULATIONS
% This script feeds the parameters for a REVOLT simulation to the
% 'sim_revolt' function.

%% START PROGRAM
clc
clear
close all

%% SIMULATION PARAMETERS
simn = 1; % Simulation number
mode = 'ol'; % AO mode ('cl' for CL, 'ol' for OL)
parentf = '/Volumes/Astro/Data/REVOLT/Simulations'; % Parent folder

%% OBSERVING PARAMETERS
timeSimu = 10; % Simulation length (s)
zenithAngle = 30; % Zenith angle (deg)
NGSmag = 5; % NGS magnitude in the WFS band

%% ATMOSPHERE PARAMETERS
r0_zenith = 0.047; % Fried parameter at zenith (m)
r0_lambda = photometry.V0; % Fried parameter wavelenght
L0 = 30; % Outer scale parameter (m)
altitude = [0, 5] * 1e3; % Layers altitude (km)
fractionalR0 = [0.9 ,0.1]; % Layers fractional Fried parameter
windSpeed = [3, 10]; % Layers wind speed (m/s)
windDirection = [0, 60];% Layers wind direction (deg)

%% TELESCOPE AND INSTRUMENT PARAMETERS
D = 1.22; % Telescope primary mirror diameter (m)
obstructionRatio = 0.1; % Central obscuration ratio
nL_cl = 16; % CLWFS number of lenslets
nL_ol = 16; % OLWFS number of lenslets
wfs_lambda_cl = photometry.J; % CLWFS band
wfs_lambda_ol = photometry.V; % CLWFS band
science_lambda = photometry.J; % Science camera band
ron_cl = 0.7; % CLWFS RON RMS (e-)
ron_ol = 2.5; % OLWFS RON RMS (e-)
nPx_cl = 24; % CLWFS number of pixels per lenslet
nPx_ol = 27; % OLWFS number of pixels per lenslet
samplingFreq_cl = 200; % CLWFS sampling frequency (Hz)
samplingFreq_ol = 200; % OLWFS sampling frequency (Hz)
fieldOfViewInArcsec = 30; % Telescope field of view (")
gain = 0.6; % CL gain
validLenslet = 0.5; % CLWFS and OLWFS lenslet filling
dmCrossCouplingCoeff = 0.3; % DM actuator coupling coefficient
nActuator = 17; % DM actuators across the pupil diameter
cam_field = 60; % Science camera field stop size (diffraction fwhm units)
nyq_cl = 2.33; % CLWFS Nyquist sampling
nyq_ol = 1; % OLWFS Nyquist sampling
pe_atm_cl = 0.85; % Atmospheric photon efficiency at the CLWFS wavelength
pe_atm_ol = 0.6; % Atmospheric photon efficiency at the OLWFS wavelength
pe_tel_cl = 0.58; % Telescope photon efficiency at the CLWFS wavelength
pe_tel_ol = 0.8; % Telescope photon efficiency at the OLWFS wavelength
pe_wfs_cl = 0.7; % CLWFS photon efficiency
pe_wfs_ol = 0.62; % OLWFS photon efficiency
pe_bs_cl = 0.9; % CLWFS beamsplitter photon efficiency 

%% COLLECT PARAMETER
params = struct('simn', simn, 'parentf', parentf, 'mode', mode, 'timeSimu', ...
    timeSimu, 'zenithAngle', zenithAngle, 'NGSmag', NGSmag, 'r0_zenith', ...
    r0_zenith, 'r0_lambda', r0_lambda, 'L0', L0, 'altitude', altitude, ...
    'fractionalR0', fractionalR0, 'windSpeed', windSpeed, 'windDirection', ...
    windDirection, 'D', D, 'obstructionRatio', obstructionRatio, 'nL_cl', ...
    nL_cl, 'nL_ol', nL_ol, 'wfs_lambda_cl', wfs_lambda_cl, 'wfs_lambda_ol', ...
    wfs_lambda_ol, 'science_lambda', science_lambda, 'ron_cl', ron_cl, ...
    'ron_ol', ron_ol, 'nPx_cl', nPx_cl, 'nPx_ol', nPx_ol, 'samplingFreq_cl', ...
    samplingFreq_cl, 'samplingFreq_ol', samplingFreq_ol, ...
    'fieldOfViewInArcsec', fieldOfViewInArcsec, 'gain', gain, ...
    'validLenslet', validLenslet, 'dmCrossCouplingCoeff', ...
    dmCrossCouplingCoeff, 'nActuator', nActuator, 'cam_field', cam_field, ...
    'nyq_cl', nyq_cl, 'nyq_ol', nyq_ol, 'pe_atm_cl', pe_atm_cl, 'pe_atm_ol', ...
    pe_atm_ol, 'pe_tel_cl', pe_tel_cl, 'pe_tel_ol', pe_tel_ol, 'pe_wfs_cl', ...
    pe_wfs_cl, 'pe_wfs_ol', pe_wfs_ol, 'pe_bs_cl', pe_bs_cl);

%% LAUNCH SIMLATION
sim_revolt(params)
