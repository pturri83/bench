% This function generates DM command map for each subaperture
function DMcmdMap = ValidDMcmdMap(Vactuator, cmd)

% vLenslet=double(wfs.wfs.validLenslet); %%copying the VLA mask
DMcmdMap=zeros(19,19);      %%generating a (16x16) empty array
% vlaInten = (wfs.wfs.lensletIntensity)'; %% Copying the subap Intensities
k=1;

for i=19:-1:1   %% loop run accounting the fact subap indices starts from lowerleft 
    for j=1:19    %% and increasing from left to right
       if Vactuator(i,j)>0  %%Only reading if VLA actuator value is non zero
             Norm = (max(cmd)-min(cmd));                  
          DMcmdMap(i,j)=cmd(k); %%Copying the command values and normalize them
          k=k+1;
       end
    end

end