wfs.wfs.camera.darkBackground= 0;
load('background01112021.mat');
load('cmd_bestflat2510.mat');
% load('slopes_FlatM_br_nvlaM.mat');
load('slopesFlatM_background_removed_0111.mat');
InterMat = zeros(size(wfs.wfs.slopes,1),277);
wfs.wfs.referenceSlopes= slopesFlatM_backgrndrem;
wfs.wfs.camera.darkBackground= savebackground;
dm.Reset();
cmd= cmd_bestflat2510;
% cmd = -1.7*focus_cmd;
dm.Send(cmd);
wfs.updateSpots;

for i=1:277; 
    tic
    fprintf('Actuator %d/%d\n',i,277); 
    cmd(i) = cmd(i)+0.1;
    dm.Send(cmd);
    pause(1);
    wfs.updateSpots;
    tempslop1 = wfs.wfs.slopes;
    cmd = cmd_bestflat2510;
    
    cmd(i) = cmd(i)-0.1;
    dm.Send(cmd);
    pause(1);
    wfs.updateSpots;
    tempslop2 = wfs.wfs.slopes;
    InterMat(:,i) = (tempslop1-tempslop2);
    cmd = cmd_bestflat2510;

    toc
end
InterMat = InterMat/0.2;
dm.Reset();