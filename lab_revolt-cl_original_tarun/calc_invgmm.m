function invgmm = calc_invgmm(inf,mask,nThresh);

nact=size(inf,3);
npix=size(inf,1);
IF3=zeros(nact,npix,npix);
for l=1:nact
        IF3(l,:,:)=inf(:,:,l);
end

IF2=transpose(reshape(IF3,nact,npix^2));

% Calculate Inverse Gamma - pupil is calculated by finding pixels that are non-zero in the difference maps
tpup=sum(sum(mask));
dum=transpose(IF2)*IF2/tpup;

[U,S,V] = svd(dum);

Sinv = zeros(nact);
for l=1:nact-nThresh
  Sinv(l,l)=1/S(l,l);
end

invgmm=V*Sinv*transpose(U);

end
