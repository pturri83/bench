
function [vlaslopesx,vlaslopesy]= SlopeFig(vLenslet, slopes)
%     vLenslet=double(wfs.wfs.validLenslet);
    vlaslopesx=zeros(16,16);
    vlaslopesy=zeros(16,16);
%     LensletInten = (wfs.wfs.lensletIntensity)';
    k=1;

    for i=16:-1:1
      for j=1:16
       if vLenslet(i,j)~=0
          vlaslopesx(i,j)=slopes(k);
          k=k+1;
         end
      end
    end
%     return vlaslopes;
end
%     imagesc(vlaInten)
% Quiverplot for each iteration
%      dataX(wfs.wfs.validLenslet) = slopes_rt(1:end/2);
%      dataY(wfs.wfs.validLenslet) = slopes_rt(end/2+1:end);
%      quiver(dataX,dataY)
%      hold on
%     else
%        break;
% %        fprintf('error');
%     end
% end
% std(slopes_rt)
% figure;
% plot(slopes_rt)
% clear ans cmd cmd_0best commandMatrix dataX dataY i slopes_rt slopesFlat 