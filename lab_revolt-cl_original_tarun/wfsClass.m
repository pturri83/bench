% Class making a WFS object working with real Andor camera frames
classdef wfsClass < handle
    properties
        cam; % Przemek/Ron's cam object

        %wfsTag;
        wfs; % OOMAO wfs object
        nLenslet = 16; %10x10 lenslets
        nPx = 88; %12 pix/subap
        nValidLenslet
        scaleZ = 1; %microns on the WF per pixel
        z; % zernike object
        q;
        figs; % figure handle for spots
        figz; % figure handle for zernikes
        itSpots; % timer for spot display
        itZern; % timer for zernike display
        %itRegisCdm; % timer for CDM registration display
        sZ4; %slope vector for unity focus error
        regis =[]; % DM registration data
        dataDir; % to save registration data
        temperature = NaN;
        tempFlag;
        raven;
        data;
        desiredSlopes;
%         flatMirror;
    end
    
    methods
        %% Constructor
        function obj = wfsClass(wfsTag, varargin)
            % create camera object and start frame streaming
            
%             obj.raven.flag = 0;
            p = inputParser;
            p.addOptional('raven', []);
            p.parse(varargin{:});         
%             if ~isempty(p.Results.raven)
%                 obj.raven = p.Results.raven;
%             end
            
            try
%                 config = raven.tools.JSON.getInstance().getJSONObject('+data/+config/config.json');
%                 obj.cam = raven.bench.WFS(config.getJSONObject(wfsTag));
%                 obj.cam.startStream;
                  obj.cam = AndorZyla();
            catch error
%                 disp(error.message);
            end
            
%                obj.temperature = obj.getParameters('temperature');
               
            % create OOMAO wfs object
%             try
                obj.wfs = shackHartmann(obj.nLenslet,obj.nLenslet*obj.nPx,0.5);
%                 obj.wfs.tag = wfsTag;
%                 obj.wfs.validLenslet = utilities.piston(obj.nLenslet);
%                 mask      = obj.wfs.validLenslet;
%                 obj.nValidLenslet = sum(mask(:));
                obj.wfs.camera.frameGrabber = @obj.andorFrameGrabberCam;
%                 obj.wfs.camera.frameGrabber = @obj.simuFrameGrabber;
                % ROI definition
%                 obj.wfs.camera.roiSouthWestCorner = [1 1]; % the image is already crop by the framegrabber
                
                % Threshold definition
                obj.wfs.framePixelThreshold = 100; %1000
                
                % Slope reference
%                obj.wfs.referenceSlopes = obj.cropedRefSlopes();
                

                



                % Define a source (needed otherwise slopesDisplay makes an error)
                
                src = source;

                tel = telescope(1.22,...
                   'obstructionRatio',0,...
                   'fieldOfViewInArcMin',0.5,...
                   'resolution',obj.nPx*obj.nLenslet,...
                   'samplingTime',1/100);
                src = src.*tel*obj.wfs;
               % obj.wfs.INIT
            [sx sy]=meshgrid(linspace(-1,1,obj.nLenslet));
%                 obj.wfs.validLenslet = utilities.piston(obj.nLenslet); % define circular mask on lenslets
                obj.wfs.validLenslet = utilities.piston(15.5,16); % define circular mask on lenslets
                % utilities.piston(15.7,16) Example to define less valid lenslet
                mask = obj.wfs.validLenslet;              
                obj.nValidLenslet = sum(mask(:));
                s = [sx(mask>0) ; sy(mask>0)];
                % we should inverse s, but we don't want to give too much weight to central
                % subaps, so we simply set all weights to 1 or -1:
                obj.sZ4 = sign(s);
                
                % Compute Zernike coefficients (needs a circular pupil)
                %obj.scaleZ = src.wavelengthInMicron/obj.wfs.lenslets.nyquistSampling/2*obj.nLenslet; % correct scale factor for PV Zernike
                obj.z = zernike(1:36,'resolution',obj.nLenslet*obj.nPx);
                obj.z=obj.z\+obj.wfs;
                obj.q = sqrt((2-(obj.z.m==0)).*(obj.z.n+1));
                
                % Listerner ON
                obj.wfs.camera.frameListener.Enabled = false;
                obj.wfs.slopesListener.Enabled = false;
                obj.wfs.intensityListener = false;
                
                % Create spot window
%                 if ~obj.raven.flag
%                     %obj.figs.fig = figure('Color','k', 'Name', [ obj.wfs.tag ' Spots']);
%                 else
%                     obj.figs.fig = obj.raven.fig.fig;
%                     subsize = .3;
%                     switch wfsTag
%                         case 'NGS1'
%                             obj.figs.subplotPos = [.003 .667 subsize subsize];
%                             %subplot(3,3,1);
%                         case 'NGS2'
%                             obj.figs.subplotPos = [.336 .003  subsize  subsize];
%                             %subplot(3,3,8);
%                         case 'NGS3'
%                             obj.figs.subplotPos = [.667 .667 subsize  subsize];
%                             %subplot(3,3,3);
%                         case 'CLWFS1'
%                             obj.figs.subplotPos = [.003 .333 subsize  subsize];
%                             %subplot(3,3,4);
%                         case 'CLWFS2'
%                             obj.figs.subplotPos = [.667 .333 subsize  subsize];
%                             %subplot(3,3,6);
%                         case 'LGS'
%                             obj.figs.subplotPos = [.336 .667 subsize  subsize];
%                             %subplot(3,3,6);
%                     end
%                     subplot('Position',obj.figs.subplotPos);
%                     colormap('pink');
%                 end
                obj.createSpotWindow
                
                %Create zernike window
                %obj.createZernWindow %this is a little annoying sometimes
                
%                 myDir = lib.utilities.getAosDir;
%                 obj.dataDir = myDir.calibDir;
                
%             catch error
%                 disp(error.message)
%                 disp(error.stack)
%             end
            
            % generate Z4 vector (one-time thing)
%             try
%                 

%             catch error
%                 disp(error.message)
%                 disp(error.stack)
%             end
%             
%             % define timers
            obj.setTimers
            
        end % end of constructor
        
        %% Destructor
        function delete(obj)
            obj.cam.delete;
            
            
%             try % Stop timers if needed
%                 switch get(obj.itSpots, 'Running');
%                     case 'on'
%                         stop(obj.itSpots)
%                 end
%                 delete(obj.itSpots)
%             catch error
%                 disp(error.message)
%                 disp(error.stack)
%             end
%             
%             try % Stop timers if needed
%                 switch get(obj.itZern, 'Running');
%                     case 'on'
%                         stop(obj.itZern)
%                 end
%                 delete(obj.itZern)
%             catch error
%                 disp(error.message)
%                 disp(error.stack)
%             end
%             
%                 obj.cam.close
%                 
% 
%             try
%                 set(obj.figs.fig,'HandleVisibility', 'on'); % allow overwriting the figure
%                 obj.wfs.delete
%                 if ~obj.raven.flag
%                     close(obj.figs.fig)
%                 end
%                 
% %                 if ishandle(obj.figz.fig)
% %                     close(obj.figz.fig)
% %                 end
%                 
%             catch error
%                 disp(error.message)
%                 disp(error.stack)
%             end
        end % end of destructor
        
        %% Frame Grabber function for OOMAO
                function out = andorFrameGrabberCam(obj)
            
%             try
                %ima = reshape(typecast(obj.cam.getFrame(1),'uint16'),128,128);
                frame = obj.cam.snap();
                out = double(frame(397:1804, 397:1804));
%                 out = double(frame(567:1974, 367:1774));
%                 out = double((reshape(frames(1).image,frames(1).height,frames(1).width)));
                
%                 out = double(reshape(typecast(frames(1).image,'uint16'),120,120));
                %out = obj.smartCropFrame(double(ima));
               
                %disp('New image acquired')
%             catch error
%                 disp(error.message)
%                 out = zeros(obj.nLenslet*obj.nPx);
%             end
        end % end of framegrabber
        
%  This function allows to use a image frame(.mat) and compute its slopes
        function out = simuFrameGrabber(obj)
            
            
            
             load('flatMirrorMat.mat')
            
            frame = Z_data;     % name of the .mat object when loaded
            
            out = frame;
            
            
            
        end
% %    Science camera image grabber     
%         function out2 =sciFrameGrabber
%         
%             vid = videoinput('gentl', 1, 'Mono8');
%             src = getselectedsource(vid);
% 
%             vid.FramesPerTrigger = 1;
% 
%             src.SensorGain = 'Gain1';
% 
%             src.ExposureTime = 50000;
% 
%             preview(vid);
% 
%             start(vid);
% 
%             stoppreview(vid);
% 
%             sci_img = getdata(vid);
%         end
        
        
        %% Update the display of the spots
        % wfs is the shackhartmann object (can be call as +wfs to grab a new frame)
        % h is the handle of the imagesc
        % hax is the handle of the axis
        
        function updateSpots(obj,varargin) % varargin is needed for the timer function
            
%             try
              
                % Update the image display
                +obj.wfs;
                
                if ishandle(obj.figs.fig)
                    set(0, 'CurrentFigure', obj.figs.fig);
                else
                    obj.createSpotWindow
                end
                
                %figure(obj.figs.fig); %select the figure
                set(obj.figs.plot, 'CData', obj.wfs.camera.frame);
                % Compute the tip/tilt directly from slopes
                slopes = obj.wfs.slopes;
                sx = slopes(1:end/2,1);
                sy = slopes(end/2+1:end);
                intensity = obj.wfs.lensletIntensity;
                tip = mean(sx(intensity>0)) - 0*(obj.nPx-1)/2;
                tilt = mean(sy(intensity>0)) - 0*(obj.nPx-1)/2;
                
                % Compute focus
%                 obj.z=obj.z\obj.wfs;
%                 focus = obj.scaleZ*obj.z.c(3)*obj.q(3);
                focus = sum(slopes.*obj.sZ4)/2/sum(obj.wfs.validLenslet(:));
                % Put together the results in the title
                maxVal = max(obj.wfs.camera.frame(:));%/14000;
                
%                 if obj.tempFlag
%                     obj.temperature = obj.getParameters('temperature');
%                 end
% 
                mytitle = sprintf('Tip=%3.2f Tilt=%3.2f Focus=%3.2f Max=%d',...
                    tip,tilt,focus,maxVal);
%                 
                 set(obj.figs.texts,'String', mytitle);
%                 
%             catch error
%                 disp(error.message)
%             end
        end % end of updateSpots
        
        %% update wavefront
        function updateWavefront(obj)
           +obj.wfs;
           figure;
           slopes = obj.wfs.slopes;
           sx = slopes(1:end/2,1);
           sy = slopes(end/2+1:end);
           gridSlopesX = zeros(obj.nLenslet, obj.nLenslet);
           gridSlopesY = zeros(obj.nLenslet, obj.nLenslet);
           gridSlopesX(obj.wfs.validLenslet==1)=sx;
           gridSlopesY(obj.wfs.validLenslet==1)=sy;
           surf(gridSlopesY);
        end
        
        %% create wavefront window
        function createWavefrontWindow(obj)
            
        end
        
        %% Observe Temp
        function observeTemp(obj, val)
            switch val
                case 0
                    obj.tempFlag = 0;
                case 1
                    obj.tempFlag = 1;
                otherwise
                    disp('only 0 for off and 1 for on are allowed');
            end
        end
        
        %% DM/WFS Registration
        function startDmRegistration(obj,dm, nActu, method)
            % method is 'threshold' or 'weighted'
            % This is the method used to compute the intersection of the
            % slope vectors
            
            nMeas = 5;
            subApInDm = 2.5; % one subap is 2.5mm in the DM plane
            switch dm.tag
                case 'CDM'
%                 case 'asdkDM'
                    actuPitch = 1.5; % in mm
                    switch nActu
                        case 1
                          pokeId = 139; % central actuator
                        otherwise
                    pokeId = [77 87 201];
                    end
                    stroke = 0.15;
                case {'SDM1', 'SDM2'}
                    actuPitch = 2.5; % in mm
                    switch nActu
                        case 1
                            pokeId = 73; % central actuator
                        otherwise
                    pokeId = [31 37 102];
                    end
                    stroke = 0.1;
                otherwise
                    disp('Invalid DM tag!')
            end
            
            
            set(obj.figs.fig,'HandleVisibility', 'on'); % allow overwriting the figure
            
            % Create a timer
            %clear regTimer
            regTimer = timer('Period', 1, 'ExecutionMode', 'FixedSpacing',...
                'TimerFcn',@dmRegistrationTimer,...
                'Name', [obj.wfs.tag 'DM Registration']);
            stop(obj.itSpots)
            start(regTimer)
            
            function dmRegistrationTimer(varargin)
                %try
                %Push actuators
                dm.coefs = zeros(dm.nActu,1);
                dm.coefs(pokeId) = stroke;
                pause(0.1)
                push =zeros(size(obj.wfs.slopes));
                for i=1:nMeas
                    % take a new frame
                    +obj.wfs;
                    push = push + obj.wfs.slopes;
                end
                %Pull actuators
                dm.coefs(pokeId) = -stroke;
                pause(0.1)
                pull = zeros(size(obj.wfs.slopes));
                for i=1:nMeas
                    % take a new frame
                    +obj.wfs;
                    pull = pull + obj.wfs.slopes;
                end
                slopes = (push-pull)/2/stroke/nMeas;
                
                % Flatten the DM
                dm.coefs = zeros(dm.nActu,1);
                
                % compute actuator coordinates
                obj.regis = lib.utilities.registration.getRegistration(pokeId,logical(dm.mask),...
                    logical(obj.wfs.validLenslet),slopes, method);
                %catch error
                %    disp(error.message)
                %end
                
                
                % scale the coordinates for the display
                dmCentre = obj.nPx*obj.nLenslet/2 *[1 1] +...
                    obj.regis.shift*obj.nPx;
                dmActu = obj.nPx*obj.nLenslet/2 * ones(size(obj.regis.actuatorCoord)) +...
                    obj.regis.actuatorCoord*obj.nPx;
                
                if length(pokeId) ==3
                    regiString = sprintf('DecX/Y= %3.2f/%3.2fmm  MagX/Y= %3.2f/%3.2fsubap/actu  Ang= %3.2f',...
                        obj.regis.shift*subApInDm,obj.regis.magXY,obj.regis.theta);
                else
                    regiString = sprintf('DecX/Y= %3.2f/%3.2fmm',...
                        obj.regis.shift*subApInDm);
                end
                obj.regis.shift = obj.regis.shift*subApInDm; % save in mm
                obj.regis.tag = [obj.wfs.tag '-' dm.tag];
                
                %display actu and DM centre position
                obj.updateSpots

                set(0, 'CurrentFigure', obj.figs.fig);
                %figure(obj.figs.fig)
                %subplot('Position',obj.figs.subplotPos);

              
                hold on
                if isempty(obj.figs.registration) % first time we display the registration
                    obj.figs.registration.center = plot(dmCentre(1),dmCentre(2),'yo', 'MarkerSize', 8, 'LineWidth', 3,'Parent', obj.figs.axis);
                    obj.figs.registration.text = text(-2,127,regiString, 'Color','w','Parent', obj.figs.axis);
                    if length(pokeId)==3;
                        obj.figs.registration.actu = plot(dmActu(1:3),dmActu(4:6),'y+', 'MarkerSize', 8, 'LineWidth', 2, 'Parent', obj.figs.axis);
                    end;

                else
                    set(obj.figs.registration.center,'XData',dmCentre(1),'YData',dmCentre(2));
                    if length(pokeId)==3;
                        set(obj.figs.registration.actu,'XData',dmActu(1:3),'YData',dmActu(4:6));
                    end
                    set(obj.figs.registration.text,'String', regiString);
                end
                %out = regis;
            end
        end
        
        %% Stop Regis timer
        function stopDmRegistration(obj)
            try
            stop(timerfind('Name', [obj.wfs.tag 'DM Registration']))
            delete(timerfind('Name', [obj.wfs.tag 'DM Registration']))
            catch error
            end
            % save the figure in a fig file
            try
            saveas(obj.figs.fig,[obj.dataDir obj.regis.tag '.fig'],'fig')
            catch error
                disp('Cannot save the registration figure.')
            end
            try
                delete(obj.figs.registration.center)
                delete(obj.figs.registration.actu)
                delete(obj.figs.registration.text)
                obj.figs.registration = [];
            catch error
            end
            try
                delete(timerfind('Name', [obj.wfs.tag 'DM Registration']))
                %set(obj.figs.fig,'HandleVisibility', 'callback'); % hide the figure handle
            catch error
                disp(error.message)
            end
            % save rigistration data on file
            if ~isempty(obj.regis);
                global regisData
                regisData = obj.regis;
                save([obj.dataDir obj.regis.tag '.mat'], 'regisData');
            end
        end
        %% get tip tilt focus
        function out = getTipTiltFocus(obj)
            try
                % take a new frame
                +obj.wfs;
                % Compute the tip/tilt directly from slopes
                slopes = obj.wfs.slopes;
                sx = slopes(1:end/2,1);
                sy = slopes(end/2+1:end);
                intensity = obj.wfs.lensletIntensity;
                tip = mean(sx(intensity>0)) - 0*(obj.nPx-1)/2;
                tilt = mean(sy(intensity>0)) - 0*(obj.nPx-1)/2;
                % Compute focus
                %                 obj.z=obj.z\obj.wfs;
                %                 focus = obj.scaleZ*obj.z.c(3)*obj.q(3);
                % Compute focus w/o Zernike
                focus2 = sum(slopes.*obj.sZ4)/2/sum(obj.wfs.validLenslet(:));
                %                 out = [tip; tilt; focus ; focus2];
                out = [tip; tilt; focus2];
            catch error
                disp(error.message)
            end
        end % end of getTipTiltFocus
        
        %% record tip tilt focus
        function out = recordTipTiltFocus(obj, n)
            data = zeros(n,3);
            for i=1:n
                data(i,:) = obj.getTipTiltFocus;
            end
            out = data;
            figure
            plot(1:n,data(:,1), 1:n,data(:,2), 1:n, data(:,3))
            grid on
            legend('Tip','Tilt', 'Focus')
            xlabel('Iterations')
            ylabel('Wavefront error [pixel]')
        end
        
        %% record tip tilt focus using time
        function recordTipTiltFocus2(obj, timeLimit)
            
            recordTTFTimer = timer('Period', .1, 'ExecutionMode', 'FixedDelay',...
                'Name', 'TTF', 'TimerFcn',@recordTTFFunction);
            state = 1;
            TTFdata = zeros(1,3);
            time = (0);
            currentTime = 0;
            t1 = tic;
            i = 1;
            
            start(recordTTFTimer);
            function recordTTFFunction(varargin)
                switch state
                    case 1
                        if currentTime<timeLimit
                            TTFdata(i,:) = obj.getTipTiltFocus;
                            currentTime = toc(t1);
                            time(i) = currentTime;
                            i=i+1;
                        else
                           state = 2;
                        end
                    case 2
                        stop(recordTTFTimer);
                        delete(recordTTFTimer);
                        
                        obj.data.TTFData = TTFdata;
                        figure
                        plot(time, TTFdata(:,1), time,TTFdata(:,2), time, TTFdata(:,3))
                        grid on
                        legend('Tip','Tilt', 'Focus')
                        xlabel('Iterations')
                        ylabel('Wavefront error [pixel]')
                end
            end
    end
        
        %% update Zernike plot
        function updateZern(obj,varargin)
            try
                
                if ishandle(obj.figz.fig)
                    set(0, 'CurrentFigure', obj.figz.fig);
                else
                    obj.createZernWindow
                end
                
                %figure(obj.figz.fig);
                obj.z=obj.z\+obj.wfs;
                set(obj.figz.plot,'Ydata',obj.scaleZ*obj.z.c.*transpose(obj.q));
                drawnow update
            catch error
                disp(error.message)
            end
        end % end of updateZern
        %% Get Zernike coeff (no update, no display)
        function out = getZernike(obj)
            obj.z=obj.z\obj.wfs;
            out.c = obj.z.c;
            out.q = transpose(obj.q);
        end
        %% Set timers
        function setTimers(obj)
            try
                obj.itSpots = timer('Period', .2, 'ExecutionMode', 'FixedSpacing',...
                    'TimerFcn',@obj.updateSpots);
                
                obj.itZern = timer('Period', .2, 'ExecutionMode', 'FixedSpacing',...
                    'TimerFcn',@obj.updateZern);
                
                
            catch error
                disp(error.message)
            end
        end % end of setTimers
        
       
        %% Create spot window
        function createSpotWindow(obj)
%             if ~obj.raven.flag
                 obj.figs.fig = figure('Color','k', 'Name', ' Spots');
%             end
            %imagesc(wfs.camera,'parent',subplot(1,1,1))
            obj.figs.plot = imagesc(obj.wfs.camera.frame);
            obj.figs.axis = gca;
            % obj.figs.texts = text(1,-2, '','Parent', obj.figs.axis, 'Color', 'w');
            obj.figs.texts = title('','Parent', obj.figs.axis, 'Color', 'w');
            axis equal tight; colormap pink;
            set(obj.figs.axis,'XTick',obj.nPx*(1:obj.nLenslet-1)+.5, ...
                'YTick', obj.nPx*(1:obj.nLenslet-1)+.5 ,'XColor','w', 'YColor', 'w',...
                'XTickLabel', '', 'YTickLabel', '', 'ZColor','w');
            grid
            % Draw the crosshair
            line(obj.nPx*[obj.nLenslet/2-1 obj.nLenslet/2+1]+.5,obj.nPx*[obj.nLenslet/2 obj.nLenslet/2]+.5,'Color','r','LineWidth',3)
            line(obj.nPx*[obj.nLenslet/2 obj.nLenslet/2]+.5,obj.nPx*[obj.nLenslet/2-1 obj.nLenslet/2+1]+.5,'Color','r','LineWidth',3)
            
            %hide the figure handle
%             if ~obj.raven.flag
            set(obj.figs.fig,'HandleVisibility', 'callback');
%             end
            obj.figs.registration = []; % no DM registration overlay
            % to update the spot display, call in a timer
            % updateSpots(+wfs,figs,h, hax)
        end
        %% Create Zernike Window
        function createZernWindow(obj)
            obj.figz.fig = figure('Color','k', 'Name', [ obj.wfs.tag ' Zernikes']);
            obj.figz.plot = stem((1:length(obj.z.c))+1,obj.scaleZ*obj.z.c.*transpose(obj.q),'yo', 'LineWidth', 2); grid
            set(gca,'XTick',(1:length(obj.z.c))+1,'Color','k','XColor','w', 'YColor', 'w');
            %ylim([-0.04 0.04]);
            
            obj.z=obj.z\obj.wfs;
            set(obj.figz.plot,'Ydata',obj.scaleZ*obj.z.c.*transpose(obj.q));
            xlim([2 22])
            xlabel('Zernike Modes');
            ylabel('WFE [\mum PTV]');
            
            % hide the figure handle
            set(obj.figz.fig,'HandleVisibility', 'callback');
        end
        
        %% Set Camera Parameters
        function setParameters(obj,field, val)
            
            timerSpot = false;
            timerZern = false;
            
            switch get(obj.itSpots, 'Running');
                    case 'on'
                        stop(obj.itSpots)
                        timerSpot = true;
            end
            switch get(obj.itZern, 'Running');
                    case 'on'
                        stop(obj.itZern)
                        timerZern = true;
            end
            par = obj.cam.getParameters;
            switch field
                case 'exposure'
                    par.exposure = val;
                case 'emccdGain'
                    par.emccdGain = val;
                case 'temperatureSetpoint'
                    par.temperatureSetpoint = val;
                case 'cooler'
                    par.coolerOn = val;
                case 'fanMode'
                    switch val
                        case 'off'
                            par.fanMode = raven.services.wfscamera.FanMode.OFF; %OFF, LOW, FULL
                        case 'low'
                            par.fanMode = raven.services.wfscamera.FanMode.LOW;
                        case 'full'
                            par.fanMode = raven.services.wfscamera.FanMode.FULL;
                        otherwise
                            disp('Unknown parameter: off, low, full')
                    end
                                     
                case 'triggerMode'
                    switch val
                        case 'internal'
                            par.triggerMode = raven.services.wfscamera.TriggerMode.INTERNAL; %EXTERNAL, EXTERNAL_START, BULB
                        case 'external'
                            par.triggerMode = raven.services.wfscamera.TriggerMode.EXTERNAL;
                        case 'externalStart'
                            par.triggerMode = raven.services.wfscamera.TriggerMode.EXTERNAL_START;
                        case 'bulb'
                            par.triggerMode = raven.services.wfscamera.TriggerMode.BULB;
                        otherwise
                            disp('Unknown parameter: internal, external, externalStart, bulb')
                    end
                    
                case 'emGainMode'
                    switch val
                        case 'dac_8_bit'
                            par.emGainMode = raven.services.wfscamera.EMGainMode.DAC_8_BIT;
                        case 'dac_12_bit'
                            par.emGainMode = raven.services.wfscamera.EMGainMode.DAC_12_BIT;
                        case 'linear'
                            par.emGainMode = raven.services.wfscamera.EMGainMode.LINEAR;
                        case 'real'
                            par.emGainMode = raven.services.wfscamera.EMGainMode.REAL;
                        otherwise
                            disp('Unknown parameter: dac_8)bit, dac_12_bit, linear, real')
                    end
                    
                case 'shutter'
                    switch val
                        case 'open'
                            par.shutter = raven.services.wfscamera.Shutter.OPEN;
                        case 'closed'
                            par.shutter = raven.services.wfscamera.Shutter.CLOSED;
                        otherwise
                            disp('Unknown parameter: open, closed');
                    end
                    
                    
                    
                    % add other parameters if needed
                otherwise
                    disp('Unknown parameter')
            end
            obj.cam.setParameter(par);
            obj.updateSpots
            
            if timerSpot
                start(obj.itSpots)
            end
            if timerZern
                start(obj.itZern)
            end
        end
        
        %% Get Camera Parameters
        function out = getParameters(obj,field)
            
            par = obj.cam.getParameters;
            switch field
                case 'exposure'
                    val = par.exposure;
                case 'emccdGain'
                    val = par.emccdGain;
                case 'temperature'
                    val = par.temperatureCurrent;
                case 'coolerStatus'
                    val = par.temperatureStatus;
                    % add other parameters if needed
                otherwise
                    disp('Unknown parameter')
            end
            out = val;
        end
        
        
        %% CroppedRefSlopes
        function out = cropedRefSlopes(obj)
            validLenslet = obj.wfs.validLenslet;
            % Construct the reference slopes for the smartly croped frame
            spotPitchInRawPixel = linspace(8.25,8.25+12.5*9,10);
            %subapPitchInRawPixel = [2 15 15+12 40 52 65 65+12 90 102 115]+6.5;
            subapPitchInRawPixel = [2 2+12 27 27+12 52 52+12 77 77+12 102 102+12]+6.5;
            
            shift = repmat(spotPitchInRawPixel-subapPitchInRawPixel,10,1);
            xRefSlope2D= ones(10)*5.5 + shift; % for full lenslet array
            yRefSlope2D= ones(10)*5.5 + shift';
            
            % You still have to raster the refslope according to the validlenslet mask:
            
            out = [xRefSlope2D(validLenslet>0) ; yRefSlope2D(validLenslet>0)];
        end
        
        %% Subap corner pixels
        function [validCornerX validCornerY] =subapCornerPixels(obj)
            validLenslet = obj.wfs.validLenslet;
            subapPitchInRawPixel = [2 2+12 27 27+12 52 52+12 77 77+12 102 102+12]+6.5;
            cornerSubap = subapPitchInRawPixel -6.5 +1;
            [cornerX cornerY] = meshgrid(cornerSubap,cornerSubap);
            validCornerX = cornerX(validLenslet>0);
            validCornerY = cornerY(validLenslet>0);
        end
    end %end of methods
    
    methods (Static)
        %% SmartCropFrame
        function out=smartCropFrame(ima)
            % create a 120x120pix image from a 128x128pix full frame
            % Ger rid of 1 pixel every 2 subap to match the 12.5pix subap pitch w/ the 300mic lenslet
            % pitch
            % One pixel=24mic, thus 12.5x24=300mic
            try
                %getRidIndex = [1 2 15 40 65 90 115 128];
                getRidIndex = [1 2 27 52 77 102 127 128];
 
                ima(getRidIndex,:)=[];
                ima(:,getRidIndex)=[];
                
                out = ima;
            catch error
                out = zeros(120);
            end
        end
        
        
    end   %% End of static methods
end

