n_act= 277;
load('influence_DM277_kate.mat')
resolution = size(influence_fun,2);
zern = zernike(2:11,1,'resolution',resolution);

phase_tip = reshape(zern.p(:,1),resolution,resolution);
phase_tilt = reshape(zern.p(:,2),resolution,resolution);
phase_focus = reshape(zern.p(:,3),resolution,resolution);
phase_astigO = reshape(zern.p(:,4),resolution,resolution);
phase_astigV = reshape(zern.p(:,5),resolution,resolution);
phase_comaV = reshape(zern.p(:,6),resolution,resolution);
phase_comaH = reshape(zern.p(:,7),resolution,resolution);
phase_trefOb = reshape(zern.p(:,8),resolution,resolution);
phase_trefV = reshape(zern.p(:,9),resolution,resolution);
new_phase_act = repmat(phase_focus,[1,1,n_act]);
figure; imagesc(phase_focus)

tmp = influence_fun(1:resolution,:,101);
mask = zeros(resolution,resolution);
mask(tmp~=0)=1;
mask_act = repmat(mask, [1,1,n_act]);
n_pix_mask = nnz(mask);

dot_prod = sum(sum(mask_act.*new_phase_act.*influence_fun(1:end-1,:,:)))/n_pix_mask;

for i = 1 : n_act % Change row
    display(['Actuator # ', num2str(i), ' / ', num2str(n_act), ...
        ' in Green''s array']) % Display the actuator number
    for k = 1 : n_act % Change column
        g_mm(i, k) = sum(sum(mask .* influence_fun(1:resolution, :, i) .* ...
            influence_fun(1:resolution, :, k))) / n_pix_mask; % Calculate the
        % Green's array element
    end
end
out_cmd = g_mm\dot_prod(:);
scaleFactor = 0.035;
out_cmd_focus = scaleFactor*out_cmd;
figure; plot(out_cmd_focus)
for i=1:277
    if (out_cmd_focus(i) > 0.2 | out_cmd_focus(i) <-0.1)
        out_cmd_focus(i)=0;
    end
end
figure; plot(out_cmd_focus)
        
        
        