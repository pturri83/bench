load('InterMat01_focusVecm1_7_051121_nVLA.mat')
[U,S,V] = svd(InterMat);
eigenValues = diag(S);
figure;
plot(eigenValues);
subplot(1,2,2)
semilogy(eigenValues,'.')
xlabel('Eigen modes')
ylabel('Eigen values')

nThresholded = 77;
iS = diag(1./eigenValues(1:end-nThresholded));
figure;
plot(iS);
[nS,nC] = size(InterMat);
iS(nC,nS) = 0;
%%
% and then the command matrix is derived.
commandMatrix = V*iS*U';

imagesc(commandMatrix)