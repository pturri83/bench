% This funtion creates the valid actuotor indexing using actuator map
% load the actuator index data
vActuator=orgdata;
% create the empty array to of right size
DM_ActuatorIndex=zeros(19,19);
k=1;

for i=19:-1:1
    for j=1:19
       if vActuator(i,j)~=0
          DM_ActuatorIndex(i,j)=k;
          k=k+1;
       end
    end
end