% This function generates Intensity map for each subaperture
function vlaIntenMap= ValidLensletIntensityMap(vLenslet, vlaInten)

% vLenslet=double(wfs.wfs.validLenslet); %%copying the VLA mask
vlaIntenMap=zeros(16,16);      %%generating a (16x16) empty array
% vlaInten = (wfs.wfs.lensletIntensity)'; %% Copying the subap Intensities
k=1;

for i=16:-1:1   %% loop run accounting the fact subap indices starts from lowerleft 
    for j=1:16    %% and increasing fro left to right
       if vLenslet(i,j)~=0    %%Only reading if VLA mask value is non zero  
          vlaIntenMap(i,j)=vlaInten(k); %%Copying the Intensity values to valid subap
          k=k+1;
       end
    end
end