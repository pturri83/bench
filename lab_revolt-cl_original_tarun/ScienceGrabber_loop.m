delete(imaqfind)
% cmd0cH = cmd0-0.2*out_cmd_comaH;
%  cmd0cV = cmd0cH+0.0*out_cmd_comaV;
 
% Science camera image grabber
iter = 11;
load('Sci_cam_backgrnd_exp50k_161221_m12.mat');
sci_cam = zeros(size(sci_cam_backgrnd, 1), size(sci_cam_backgrnd, 2),iter);
metric = zeros(iter,1);
imaqtool;
vid = videoinput('gentl', 1, 'Mono12');
% triggerconfig(vid,'manual');
triggerconfig(vid, 'hardware', 'DeviceSpecific', 'DeviceSpecific');

% set(vid,'TriggerRepeat',inf);
% vid.FramesPerTrigger = 1;
vid.TriggerRepeat = Inf;
vid.FramesPerTrigger = 1;

dm.Reset
pause(1)

% vid.ROIPosition = [90 0 230 256];
% 
% vid.ROIPosition = [90 60 230 196];
% 
% vid.ROIPosition = [90 60 230 196];
% 
% vid.ROIPosition = [90 60 120 196];
% 
% vid.ROIPosition = [90 60 120 120];

src = getselectedsource(vid);

src.SensorGain = 'Gain1';

src.ExposureTime = 50000;

start(vid);

for i=1:iter
        j = -1:0.1:1.0;
        cmdN = cmd0+j(i)*out_cmd_focus;
        
        if (-0.5 <min(cmdN) && max(cmdN)<0.5)
            dm.Send(cmdN);
            pause(0.5);
            snapshot = getsnapshot(vid);

            % pause(1);
            sci_cam(:,:,i) = double(snapshot)-sci_cam_backgrnd;

            % sci_cam_backgrndm12 = sci_img;
%             temp_img = double(sci_img);
%             temp_sci = double(sci_img)-mode(temp_img)
%             sci_cam(:,:,i) = double(sci_img)-mode(temp_img(:));
%             sci_cam(:,:,i) = double(sci_img)-sci_cam_backgrnd;
%             sci_cam(:,:,i) = sci_cam_temp - mode(sci_cam_temp(:));
            tempsci = sci_cam(:,:,i);
%             pause(1);
            figure; imagesc(tempsci)
            
%             pause(1);
            
            maxsci= max(tempsci(:));
            sumsci= sum(tempsci(:));
            
            fprintf('Max value %.3f \n',maxsci );
            fprintf('total value %.3f \n',sumsci );
            format long
            metric(i)= sumsci/maxsci;
            fprintf('Metric %.3f \n',i, metric(i));
    %         plot( i,metric(i) )
    %         cmdN = cmd0;
%         end
        end
end
figure; plot(j', metric)
stop(vid)
delete(vid)
