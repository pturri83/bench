% Converts 3-D array sci_cam to a movie file movie_focus.avi with frame
% rate 1/persec

VidObj = VideoWriter('movie_comaH_trefV_focus.avi', 'Uncompressed AVI'); %set your file name and video compression
VidObj.FrameRate = 1; %set your frame rate
open(VidObj);
for f = 1:size(sci_cam, 3)  %sci_cam is your 3D matrix
    writeVideo(VidObj,mat2gray(sci_cam(:,:,f)));
end
close(VidObj);