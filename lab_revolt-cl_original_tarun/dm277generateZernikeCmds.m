load('influence_1.mat')
resolution = size(influence_fun,2);
zern = zernike(2:4,1,'resolution',resolution);

phase_tip = reshape(zern.p(:,1),resolution,resolution);
phase_tilt = reshape(zern.p(:,2),resolution,resolution);
phase_focus = reshape(zern.p(:,3),resolution,resolution);
new_phase_act = repmat(phase_focus,[1,1,n_act]);

tmp = influence_fun(1:resolution,:,101);
mask = zeros(resolution,resolution);
mask(tmp~=0)=1;
mask_act = repmat(mask, [1,1,n_act]);
n_pix_mask = nnz(mask);

dot_prod = sum(sum(mask_act.*new_phase_act.*influence_fun(1:end-1,:,:)))/n_pix_mask;

for i = 1 : n_act % Change row
    display(['Actuator # ', num2str(i), ' / ', num2str(n_act), ...
        ' in Green''s array']) % Display the actuator number
    for k = 1 : n_act % Change column
        g_mm(i, k) = sum(sum(mask .* influence_fun(1:resolution, :, i) .* ...
            influence_fun(1:resolution, :, k))) / n_pix_mask; % Calculate the
        % Green's array element
    end
end
out_cmd = g_mm\dot_prod(:);
scaleFactor = 0.035;
out_cmd_scaled = scaleFactor*out_cmd;
figure
plot(out_cmd_scaled)