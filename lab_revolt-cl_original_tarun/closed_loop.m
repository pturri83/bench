% make sure DM is powered on!!
wfs.wfs.camera.darkBackground= 0;
load('best_flat_cmd171121.mat');
load('background01112021.mat');
load('slopes_rt_171121.mat')
% load('cmd_bestflat2510.mat');
load('slopes_FlatM_br_nvlaM.mat');
load('ComMat_nVLA_Md150_051121_backrem.mat');
load('DM277_ActutorMap.mat');

wfs.wfs.camera.darkBackground= savebackground;
dataX = zeros(size(wfs.wfs.validLenslet));
dataY = zeros(size(wfs.wfs.validLenslet));
figure;
% f1 = figure;
% f2 = figure;
% f3 = figure;
% dm =asdkDM('alpaoHSDM277-15-005');
% wfs= wfsClass();
dm.Reset;
pause(1);
wfs.wfs.referenceSlopes= slopesFlatMir_br_nVLA+slopes_rt1711;
% wfs.wfs.referenceSlopes= slopes_rt_eyeDr1401;
wfs.updateSpots;
slopes_rt = wfs.wfs.slopes;
% cmd = zeros(277,1);
cmd = cmd_bestflat1711
% cmd = cmd0cH;
% cmd(155)=0.05;
%cmd(230) = 0.05;
% cmd(43)=0.05;
for i=1:50
    if (-0.5 <min(cmd) && max(cmd)<0.5)
     DMcmdMap = ValidDMcmdMap(DM277_ActuatorMap, cmd);
%      set(0, 'CurrentFigure', f1)
     subplot(1,3,1);
     imagesc(fliplr(DMcmdMap'));
     axis square;

%      colorbar;
     dm.Send(cmd);
     pause(1);
     wfs.updateSpots;
     slopes_rt = wfs.wfs.slopes;
     fprintf('RMS %.3f \n',rms(slopes_rt));
     err= commandMatrix*slopes_rt;
     fprintf('RMS (err) %.3f \n',rms(err));
%      plot(err)
%      hold on
     cmd= cmd-0.50*err;  
     fprintf('cmd %.3f \n',min(cmd), max(cmd));
%      DMcmdMap = ValidDMcmdMap(DM277_ActuatorMap, cmd);
%      % Plotting valid DM command map 
%       set(0, 'CurrentFigure', f1)
%      imagesc(DMcmdMap)
% Plotting slopes for each iteration   
%      set(0, 'CurrentFigure', f1)
%      plot(slopes_rt)
%      pause(0.5);
%      hold on
% Plotting intensity map for valid Subap
    vlaIntenMap= ValidLensletIntensityMap(wfs.wfs.validLenslet, wfs.wfs.lensletIntensity);
%     set(0, 'CurrentFigure', f2)
    subplot(1,3,2);
    imagesc(fliplr(vlaIntenMap'));
    axis square
%     colorbar;
%     figure;
%     set(0, 'CurrentFigure', f1)
    subplot(1,3,3);
    dataX(wfs.wfs.validLenslet) = slopes_rt(1:end/2);
    dataY(wfs.wfs.validLenslet) = slopes_rt(end/2+1:end);
    quiver(dataX,dataY);
    axis square
    hold on
% Quiverplot for each iteration
    
%     hold on
    else
       break;
%        fprintf('error');
    end
end

% figure;
% plot(slopes_rt)
% clear ans cmd cmd_0best commandMatrix dataX dataY i slopes_rt slopesFlat 