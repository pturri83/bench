delete(imaqfind)
% Science camera image grabber
imaqtool;
load('Sci_cam_backgrnd_exp50k_161221_m12.mat');



vid = videoinput('gentl', 1, 'Mono12');
% triggerconfig(vid,'manual');
triggerconfig(vid, 'hardware', 'DeviceSpecific', 'DeviceSpecific');
% set(vid, 'HardwareTriggering', 'On');
% set(vid.DeviceProperties, 'FrameStartTriggerActivation', 'RisingEdge');
% set(vid,'TriggerRepeat',inf);
vid.TriggerRepeat = Inf;
vid.FramesPerTrigger = 1;
% vid.ROIPosition = [90 0 230 256];
% 
% vid.ROIPosition = [90 0 230 256];
% 
% vid.ROIPosition = [90 60 230 196];
% 
% vid.ROIPosition = [90 60 230 196];
% 
% vid.ROIPosition = [90 60 120 196];
% 
% vid.ROIPosition = [90 60 120 120];

src = getselectedsource(vid);

src.SensorGain = 'Gain1';

src.ExposureTime = 50000;

start(vid);


snapshot = getsnapshot(vid);
%subtracting the background from sci image
sci_fr = double(snapshot)-sci_cam_backgrnd; 
figure; imagesc(sci_fr)

% sci_cam_backgrndm12 = sci_img;
% sci_cam = double(sci_img);
% % -double(sci_cam_backgrndm12)
% figure; imagesc(sci_cam)

maxsci= max(sci_fr(:));
sumsci= sum(sci_fr(:));

fprintf('Max value %.3f \n',maxsci );
fprintf('total value %.3f \n',sumsci );
% format long
fprintf('Metric %.3f \n', sumsci/maxsci  );

stop(vid)
delete(vid)
