Handling buffer timeout
camera = AndorZyla()
>> camera.stopAcquisition;
>> camera.startAcquisition;
>> array = camera.waitBuffer(1000*camera.ExposureTime+500);
>> camera.queueBuffer;
>> image = camera.convertMono12ToMatrix(array, camera.AOIHeight, camera.AOIWidth, camera.AOIStride);
>> imagesc(image)
>> camera.stopAcquisition;