% cmd0f = cmd0+0.4*out_cmd_focus;
% cmd0cV = cmd0f+0.2*out_cmd_comaV;
% cmd0cH = cmd0cV+0.4*out_cmd_comaH;
% Science camera image grabber
iter = 11;
load('Sci_cam_backgrnd_exp50k_161221_m12.mat');
sci_cam = zeros(size(sci_cam_backgrnd, 1), size(sci_cam_backgrnd, 2),iter);
metric = zeros(iter,1);
imaqtool;
vid = videoinput('gentl', 1, 'Mono12');
triggerconfig(vid,'manual');
set(vid,'TriggerRepeat',inf);
vid.FramesPerTrigger = 1;



% vid.ROIPosition = [90 0 230 256];
% 
% vid.ROIPosition = [90 60 230 196];
% 
% vid.ROIPosition = [90 60 230 196];
% 
% vid.ROIPosition = [90 60 120 196];
% 
% vid.ROIPosition = [90 60 120 120];

src = getselectedsource(vid);

src.SensorGain = 'Gain1';

src.ExposureTime = 50000;

start(vid);

for i=1:iter
        j = -1:0.2:1;
        cmdN = cmd0f+j(i)*out_cmd_comaH;
        
        if (-0.5 <min(cmdN) && max(cmdN)<0.5)
            dm.Send(cmdN);
            pause(1);
            trigger(vid);

            % pause(1);
            sci_img = getdata(vid);

            % sci_cam_backgrndm12 = sci_img;
%             temp_img = double(sci_img);
%             temp_sci = double(sci_img)-mode(temp_img)
%             sci_cam(:,:,i) = double(sci_img)-mode(temp_img(:));
            sci_cam(:,:,i) = double(sci_img)-sci_cam_backgrnd;
%             sci_cam(:,:,i) = sci_cam_temp - mode(sci_cam_temp(:));
            tempsci = sci_cam(:,:,i);
            pause(1);
            figure; imagesc(sci_cam(:,:,i))
            pause(1);
            
            maxsci= max(tempsci(:));
            sumsci= sum(tempsci(:));
            
            fprintf('Max value %.3f \n',maxsci );
            fprintf('total value %.3f \n',sumsci );
            format long
            metric(i)= maxsci/sumsci;
            fprintf('Metric %.3f \n',metric(i)  );
    %         plot( i,max(sci_cam(:))/sum(sci_cam(:))     )
    %         cmdN = cmd0;
%         end
end
stop(vid)