% load('InterMat01_focusVm1_7.mat');
% load('VLAMap.mat')
% This function find each x and y slopes and plots them in the vla Map 
function [vlaslopesx,vlaslopesy]= SlopeFig(VLAMap, slopes)
    figure;
    vlaslopesx=zeros(16,16);
    vlaslopesy=zeros(16,16);
    k=1;

    for i=16:-1:1
      for j=1:16
         if VLAMap(i,j)~=0
          vlaslopesx(i,j)=slopes(k);
          vlaslopesy(i,j)=slopes(end/2+k);
          k=k+1;
         end
      end
    end
    subplot(1,2,1);
    imagesc(fliplr(vlaslopesx'));
    subplot(1,2,2);
    imagesc(fliplr(vlaslopesy'));
end

