% cmd0f = cmd0+0.4*out_cmd_focus;
% cmd0cV = cmd0f+0.2*out_cmd_comaV;
% cmd0cH = cmd0cV+0.4*out_cmd_comaH;
% Science camera image grabber

iter = 11;
load('Sci_cam_backgrnd_exp50k_161221_m12.mat');
sci_cam = zeros(size(sci_cam_backgrnd, 1), size(sci_cam_backgrnd, 2),iter);
metric = zeros(iter,1);

imaqtool;
vid = videoinput('gentl', 1, 'Mono12');
% triggerconfig(vid,'manual');
triggerconfig(vid, 'hardware', 'DeviceSpecific', 'DeviceSpecific');
% set(vid, 'HardwareTriggering', 'On');
% set(vid.DeviceProperties, 'FrameStartTriggerActivation', 'RisingEdge');
% set(vid,'TriggerRepeat',inf);
vid.TriggerRepeat = Inf;
vid.FramesPerTrigger = 1;



src = getselectedsource(vid);

src.SensorGain = 'Gain1';

src.ExposureTime = 50000;

start(vid);

for i=1:iter

%             trigger(vid);
            snapshot = getsnapshot(vid);

            sci_cam(:,:,i) = double(snapshot)-sci_cam_backgrnd;
            tempsci = sci_cam(:,:,i);
% 
            figure; imagesc(tempsci)  
            maxsci= max(tempsci(:));
            sumsci= sum(tempsci(:));
% %             
            fprintf('Max value %.3f \n',maxsci );
            fprintf('total value %.3f \n',sumsci );
            metric(i)= maxsci/sumsci;
            fprintf('Metric %.3f \n',metric(i)  );

end


% Call the STOP function to stop the device.
stop(vid)
delete(vid)
