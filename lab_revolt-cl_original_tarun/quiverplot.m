% This function generates quiverplot for vla 
function quiverplot(vLenslet,slopes_rt)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
dataX = zeros(size(vLenslet));  % creating an empty array of valid lenslet array size
dataY = zeros(size(vLenslet));   
dataX(vLenslet) = slopes_rt(1:end/2);
dataY(vLenslet) = slopes_rt(end/2+1:end);
quiver(dataX,dataY)
end

