% project_phase will return the actuator commands needed to fit
% phasescreen (pScreen) given a set of influence functions (infBin)
%
% Inputs
% inf - Influence Functions - 3 D matrix.  Will have dimensions npix x npix x nactuators
% aperture - defines aperture mask (1 and 0) with dimensions npix x npix
% pScreen - Phase as measured in the same dimensions as the inf.  The matrix will have npix x npix dimensions
% invgmm - Gamma inverse calculated first by calc_invgmm.m
function act = project_phase(inf,aperture,pScreen,invgmm)

nact=size(inf,3);
npix=size(inf,1);

IF2=reshape(inf,npix^2,nact);
tpup=sum(sum(aperture));

% Calculate Inverse Gamma - pupil is calculated by finding pixels that are non-zero in the difference maps

pScreen=pScreen.*aperture;

act=invgmm*transpose(IF2)*reshape(pScreen,npix^2,1)/tpup;

end
