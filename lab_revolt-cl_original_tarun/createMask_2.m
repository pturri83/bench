vLenslet=wfs.wfs.validLenslet;
refMask=-ones(16*21);
k=0;
subap=ones(21);
for i=1:16
    l=(i-1)*21+1;
    for j=1:16
       m=(j-1)*21+1;
       if vLenslet(i,j)==1
          refMask(m:m+20,l:l+20)=subap*k;
          k=k+1;
       end
    end
end
padrefMask = padarray(refMask, [88,88], -1);