% This function generates valid lenslet index map 
vLenslet=double(wfs.wfs.validLenslet);
vlaIndex=zeros(16,16);

k=1;

for i=16:-1:1
    for j=1:16
       if vLenslet(i,j)~=0
          vlaIndex(i,j)=k;
          k=k+1;
       end
    end
end
figure; imagesc(vlaIndex)