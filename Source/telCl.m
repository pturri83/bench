%% TELESCOPE
% Class to simulate the DAO 1.22 telescope for the REVOLT bench.
% Written by Paolo Turri.

%#ok<*VUNUS>

classdef telCl < handle
    
    properties
        d = 1.22; % Diameter [m]
        obstructionRatio = 0.1; % Central obscuration ratio
    end
end
