%% MIRROR
% Class to manage a REVOLT mirror.
% Written by Paolo Turri.

%#ok<*VUNUS>

classdef (Abstract) mirrorCl < handle
    
    properties (Abstract)
        tag; % Mirror name tag
        figc_size; % Actuators figure size [px]
            % [monitor #, left, bottom, width, height]
        n_act; % Total numer of actuators
        n_act_d; % Numer of actuators on the diameter
        n_act_d_p; % Numer of actuators on the pupil diameter
        pitch; % Pitch on the telescope aperture [m]
        infunc; % Influence functions
        mask; % Actuators mask
        cmd; % Last command applied
        cmd_zero; % Zero command
        cmd2d; % 2D command array
        figc; % Handle for actuators figure
    end
    
    properties
        cross_couple = 0.4; % Expected actuator cross-coupling
        settle_t = 0.1; % Settling time [s]
        updatePlots = true; % Show and update figures
    end
    
	methods (Abstract)
        setCmd(obj, cmd);
        reset(obj);
    end
    
    methods
        %% Create actuators plot
        function createActPlot(obj)
            %{
            Create the window that shows the actuators.
            %}
            
            obj.figc.fig = benchCl.newFig(obj.figc_size, ...
                                          sprintf('%s Actuators', obj.tag));
            obj.figc.plot = imagesc(zeros(obj.n_act));
            obj.figc.axis = gca;
            axis equal tight;
            colormap hot;
            cbar = colorbar;
            cbar.Color = 'w';
            set(obj.figc.axis, 'XColor', 'w', 'YColor', 'w', 'ZColor', 'w');
            set(obj.figc.fig, 'HandleVisibility', 'callback');
        end
        
        %% Update actuators plot
        function updateAct(obj)
            %{
            Update the actuators plot.
            %}
            
            if ~isprop(obj, 'figc')
                obj.createActPlot;
            end
            
            set(obj.figc.plot, 'CData', obj.cmd2d);
            drawnow limitrate;
        end
    end
end
