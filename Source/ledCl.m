%% LED
% Class to simulate the LED for the REVOLT bench.
% Written by Paolo Turri.

%#ok<*VUNUS>

classdef ledCl < sourceCl
    
    properties
        lambda = 660e-9; % Central wavelength [m]
    end
    
    methods (Static)
        %% On
        function turnon
            %{
            Turn on the light source.
            %}
            
            disp('Turn on LED. Press ENTER when ready...');
            pause;
        end
        
        %% Off
        function turnoff
            %{
            Turn off the light source.
            %}
            
            disp('Turn off LED. Press ENTER when ready...');
            pause;
        end
    end
end
