%% Goldeye
% Class to manage a Allied Vision's Goldeye G-008 Cool TEC1 detector.
% Written by Paolo Turri.

%#ok<*VUNUS>

classdef goldeyeCl < detectCl
    
    properties
        x_px = 320; % Width of the detector [px]%%%%%%%%%%%%%%%%%
        y_px = 256; % Height of the detector [px]%%%%%%%%%%%%%%%%%%
        gain = 0.067; % Gain [e-/ADU]
        ron = 170; % RON RMS [e-]
        satur = 65536; % Saturation level [ADU]%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        gain_mode = 'Gain1'; % Gain mode ('Gain0', 'Gain1')
        
        wd; % Working directory
        tag_use; % Tag to describe the use of the detector (for saving files)
        n_px; % Total number of pixels
        exp_t; % Exposure time [s]
        cal_n; % Number of exposures for the dark frame and flat field
            % calibrations
        dark_t; % Exposure time of individual exposures for the dark frame
            % calibration [s]
        flat_t; % Exposure time of individual exposures for the flat field
            % calibration [s]
        frame; % Last frame
        dark; % Dark frame
        flat; % Flat field
        handle; % Detector handle
        
        vid; % Video input object
        src; % Video source object
    end
    
    methods
        %% Constructor
        function obj = goldeyeCl(detect_handle, cal_n, dark_t, flat_t, wd, ...
                                 tag_use)
            %{
            Build object.
            
            Parameters:
            detectorHandle (int): Detector handle
            cal_n (int): Number of exposures for the dark frame and flat field
                calibrations
            dark_t (float): Exposure time of individual exposures for the dark
                frame calibration [s]
            flat_t (float): Exposure time of individual exposures for the flat
                field calibration [s]
            wd (str): Working directory
            tag_use (str): Tag to describe the use of the detector (for saving
                files)
            %}
            
            obj.wd = wd;
            obj.cal_n = cal_n;
            obj.dark_t = dark_t;
            obj.flat_t = flat_t;
            obj.handle = detect_handle;
            obj.n_px = obj.x_px * obj.y_px;
            obj.tag_use = tag_use;
            obj.connect;
            obj.cal_init;
        end
        
        %% Connect
        function connect(obj)
            %{
            Connect and initialize the detector.
            %}
            
            obj.vid = videoinput('gentl', obj.handle, 'Mono8');
            obj.vid.FramesPerTrigger = 1;
            obj.src = getselectedsource(obj.vid);
            obj.src.SensorGain = obj.gain_mode; %%%%%%%%%%%%%%%%%what is this for?
            obj.set_exp_t(1);
            start(obj.vid);
        end
        
        %% Disconnect
        function disconnect(obj)
            %{
            Disconnect the detector.
            %}
            
            stop(obj.vid);
        end
        
        %% Set exposure time
        function set_exp_t(obj, time)
            %{
            Set the detector exposure time.
            
            Parameters:
            time (float): Exposure time [s]
            %}
            
            obj.exp_t = time;
            obj.src.ExposureTime = time * 1;%%%%%%%%%%%%%%%%%%%%%%should it be *1000?
        end
        
        %% Get frame
        function frame = get_frame_raw(obj)
            %{
            Take an image with the detector.
            
            Output:
            frame (arr[float]): Image
            %}
            
            frame = getdata(obj.vid);%%%%%%%%%%%%%is it float?
            obj.frame = frame;
        end
    end
end
