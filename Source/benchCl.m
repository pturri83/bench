%% BENCH
% Class to manage the REVOLT bench.
% Written by Paolo Turri.

%#ok<*VUNUS>

classdef benchCl < handle
    
    properties
        comm_poke = 0.1; % Calibration command
        comm_poke_n = 10; % Number of calibration push-and-pull
        wd = 'C:\Paolo\Data'; % Working directory
        
        tel; % Telescope object
        led; % LED calibration source object
        lamp; % Lamp calibration source object
        clwfs; % CL WFS object
        olwfs; % OL WFS object
        slm; % SLM object
        dm; % DM object
        sci; % Science camera object
    end
    
    methods
        %% Constructor
        function obj = benchCl
            %{
            Build object.
            %}
            
            if ~exist(fullfile(obj.wd, 'cal'), 'dir')
                mkdir(fullfile(obj.wd, 'cal'));
            end
            
            obj.tel = telCl;
            obj.led = ledCl;
            obj.lamp = lampCl;
            %obj.slm = slmCl(obj.source, obj.tel);
            obj.dm = dmCl(obj.tel);
            obj.clwfs = wfsCl('cl', obj.tel, obj.wd);
            obj.sci = sciCl(obj.wd);
            %obj.cldmloop = loopCl();%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        end
        
        %% Destructor
        function delete(obj)
            %{
            Delete object.
            %}
            
            obj.lamp.delete;
            %obj.slm.delete;
            obj.dm.delete;
            obj.clwfs.delete;
            obj.sci.delete;
        end
    end
    
    methods (Static)
        %% New figure
        function handle = newFig(size, title)
            %{
            Create a new figure.
            
            Parameters:
            size (arr[int]): % Figure size [px]
                [monitor #, left, bottom, width, height]
            title (str): Title
            
            Output:
            handle (fig): Figure handle
            %}
            
            monitors = get(0, 'MonitorPositions');
            fig_pos = [(monitors(size(1, 1), 1) + size(2)), ...
                       (monitors(size(1, 1), 2) + size(3)), size(4), size(5)];
            handle = figure('Position', fig_pos, 'Color', 'k', 'Name', ...
                            title, 'NumberTitle', 'off');
        end
    end
end
