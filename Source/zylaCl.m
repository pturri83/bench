%% Zyla
% Class to manage an Andor's Zyla 5.5 USB 3.0 detector.
% Written by Paolo Turri.

%#ok<*VUNUS>

classdef zylaCl < detectCl
    
    properties
        x_px = 2560; % Width of the detector [px]
        y_px = 2160; % Height of the detector [px]
        gain = 0.45; % Gain [e-/ADU]
        ron = 2.6; % RON RMS [e-]
        satur = 65535; % Saturation level [ADU]
        trigger_mode = 'Internal'; % Trigger mode ('Internal', 'Software',
            % 'External', 'External Start', 'External Exposure')
        cycle_mode = 'Fixed'; % Acquisition mode ('Fixed', 'Continuous')
        shutter_mode = 'Global'; % Shutter mode ('Rolling', 'Global')
        encode_mode = 'Mono16'; % Bit format ('Mono12', 'Mono12Packed',
            % 'Mono16', 'Mono32')
        gain_mode = '16-bit (low noise & high well capacity)'; % Pre-amp gain
            % ('11-bit (high well capacity)', '12-bit (high well capacity)',
            % '11-bit (low noise)', '12-bit (low noise)',
            % '16-bit (low noise & high well capacity)'
        timeout = 0.5; % Time to wait for an exposure [s]
        
        wd; % Working directory
        tag_use; % Tag to describe the use of the detector (for saving files)
        n_px; % Total number of pixels
        exp_t; % Exposure time [s]
        cal_n; % Number of exposures for the dark frame and flat field
            % calibrations
        dark_t; % Exposure time of individual exposures for the dark frame
            % calibration [s]
        flat_t; % Exposure time of individual exposures for the flat field
            % calibration [s]
        frame; % Last frame
        dark; % Dark frame
        flat; % Flat field
        handle; % Detector handle
        
        handle_sdk; % Detector handle used by the Andor SDK
        size_bytes; % Image size in bytes
        row_bytes; % Row size in bytes
    end
    
    methods
        %% Constructor
        function obj = zylaCl(detect_handle, cal_n, dark_t, flat_t, wd, tag_use)
            %{
            Build object.
            
            Parameters:
            detect_handle (int): Detector handle
            cal_n (int): Number of exposures for the dark frame and flat field
                calibrations
            dark_t (float): Exposure time of individual exposures for the dark
                frame calibration [s]
            flat_t (float): Exposure time of individual exposures for the flat
                field calibration [s]
            wd (str): Working directory
            tag_use (str): Tag to describe the use of the detector (for saving
                files)
            %}
            
            obj.wd = wd;
            obj.cal_n = cal_n;
            obj.dark_t = dark_t;
            obj.flat_t = flat_t;
            obj.handle = detect_handle;
            obj.n_px = obj.x_px * obj.y_px;
            obj.tag_use = tag_use;
            obj.connect;
            obj.cal_init;
        end
        
        %% Connect
        function connect(obj)
            %{
            Connect and initialize the detector.
            %}
            
            [rc] = AT_InitialiseLibrary();
            AT_CheckError(rc);
            [rc, obj.handle_sdk] = AT_Open(obj.handle);
            AT_CheckError(rc);
            [rc] = AT_SetBool(obj.handle_sdk, 'SensorCooling', 1);
            AT_CheckError(rc);
            [rc] = AT_SetInt(obj.handle_sdk, 'AOIWidth', obj.x_px);
            AT_CheckWarning(rc);
            [rc] = AT_SetInt(obj.handle_sdk, 'AOIHeight', obj.y_px);
            AT_CheckWarning(rc);
            [rc] = AT_SetInt(obj.handle_sdk, 'AOILeft', 1);
            AT_CheckWarning(rc);
            [rc] = AT_SetInt(obj.handle_sdk, 'AOITop', 1);
            AT_CheckWarning(rc);
            [rc, obj.row_bytes] = AT_GetInt(obj.handle_sdk, 'AOIStride');
            AT_CheckError(rc);
            [rc] = AT_SetEnumString(obj.handle_sdk, 'TriggerMode', ...
                                    obj.trigger_mode);
            AT_CheckError(rc);
            [rc] = AT_SetEnumString(obj.handle_sdk, 'PixelEncoding', ...
                                    obj.encode_mode);
            AT_CheckError(rc);
            [rc] = AT_SetEnumString(obj.handle_sdk, 'CycleMode', ...
                                    obj.cycle_mode);
            AT_CheckError(rc);
            [rc] = AT_SetEnumString(obj.handle_sdk, ...
                                    'ElectronicShutteringMode', ...
                                    obj.shutter_mode);
            AT_CheckError(rc);
            [rc] = AT_SetEnumString(obj.handle_sdk, ...
                                    'SimplePreAmpGainControl', ...
                                    obj.gain_mode);
            AT_CheckError(rc);
            [rc, obj.size_bytes] = AT_GetInt(obj.handle_sdk, 'ImageSizeBytes');
            AT_CheckError(rc);
            obj.set_exp_t(1);
        end
        
        %% Disconnect
        function disconnect(obj)
            %{
            Disconnect the detector.
            %}
            
            [rc] = AT_SetBool(obj.handle_sdk, 'SensorCooling', 0);
            AT_CheckError(rc);
            [rc] = AT_Close(obj.handle_sdk);
            AT_CheckWarning(rc);
            [rc] = AT_FinaliseLibrary();
            AT_CheckWarning(rc);
        end
        
        %% Set exposure time
        function set_exp_t(obj, time)
            %{
            Set the detector exposure time.
            
            Parameters:
            time (float): Exposure time [s]
            %}
            
            obj.exp_t = time;
            [rc] = AT_SetFloat(obj.handle_sdk, 'ExposureTime', time);
            AT_CheckError(rc);
        end
        
        %% Get frame
        function frame = get_frame_raw(obj)
            %{
            Take an image with the detector.
            
            Output:
            frame (arr[float]): Image
            %}
            
            [rc, buf] = obj.start_buf;
            
            while rc == 13
                fprintf('Waiting for acquisition by Zyla #%d...', obj.handle);
                obj.stop_buf;
                [rc, buf] = obj.start_buf;
            end
            
            %obj.queueBuffer();%%%maybe this is needed
            obj.stop_buf;
            [rc, frame] = AT_ConvertMono16ToMatrix(buf, obj.y_px, obj.x_px, ...
                                                   obj.row_bytes);
            AT_CheckWarning(rc);
            frame = double(rot90(frame));
            obj.frame = frame;
        end
        
        %% Start buffer
        function [rc, buf] = start_buf(obj)
            %{
            Start taking an image in the buffer.
            
            Output:
            rc (int): Return code
            buf (arr[float]): Buffer
            %}
            
            [rc] = AT_QueueBuffer(obj.handle_sdk, obj.size_bytes);
            AT_CheckWarning(rc);
            [rc] = AT_Command(obj.handle_sdk, 'AcquisitionStart');
            AT_CheckWarning(rc);
            [rc, buf] = AT_WaitBuffer(obj.handle_sdk, ...
                                      ((obj.exp_t + obj.timeout) * 1000));
            AT_CheckWarning(rc);
        end
        
        %% Stop buffer
        function stop_buf(obj)
            %{
            Stop taking an image from the buffer.
            %}
            
            [rc] = AT_Command(obj.handle_sdk, 'AcquisitionStop');
            AT_CheckWarning(rc);
            [rc] = AT_Flush(obj.handle_sdk);
            AT_CheckWarning(rc);
        end
    end
end
