%% DETECTOR
% Class to manage a REVOLT detector.
% Written by Paolo Turri.

%#ok<*VUNUS>

classdef (Abstract) detectCl < handle
    
    properties (Abstract)
        wd; % Working directory
        tag_use; % Tag to describe the use of the detector (for saving files)
        x_px; % Width of the detector [px]
        y_px; % Height of the detector [px]
        n_px; % Total number of pixels
        gain; % Gain [e-/ADU]
        ron; % RON RMS [e-]
        satur; % Saturation level [ADU]
        exp_t; % Exposure time [s]
        cal_n; % Number of exposures for the dark frame and flat field
            % calibrations
        dark_t; % Exposure time of individual exposures for the dark frame
            % calibration [s]
        flat_t; % Exposure time of individual exposures for the flat field
            % calibration [s]
        frame; % Last frame
        dark; % Dark frame
        flat; % Flat field
        handle; % Detector handle
    end
    
	methods (Abstract)
        set_exp_t(obj);
        get_frame_raw(obj);
    end
    
    methods (Abstract, Static)
        connect;
        disconnect;
    end
    
    methods
        %% Destructor
        function delete(obj)
            %{
            Delete object.
            %}
            
            obj.disconnect;
        end
        
        %% Get frame
        function frame_out = get_frame(obj, varargin)
            %{
            Take an image with the detector, withs the option to reduce it with
            the dark/background frame and/or the flat field.
            
            Parameters:
            reduce (str): Method of image reduction: 'none', 'dark', 'flat',
                'both' (default)
            report_max (bool): Report maximum raw value: true, false (default)
            
            Output:
            frame (arr[float]): Image
            %}
            
            p = inputParser;
            addParameter(p, 'reduce', 'both', @ischar);
            addParameter(p, 'report_max', false, @islogical);
            parse(p, varargin{:});
            frame_out = get_frame_raw(obj);
            
            if any(frame_out == obj.satur)
                warning(['Frame saturated (',  obj.tag_use, ')'])
            end
            
            if p.Results.report_max
                fprintf('Raw image values: maximum = %u, median = %u \n', ...
                        max(frame_out(:)), median(frame_out(:)));
            end
            
            switch p.Results.reduce
                case 'dark'
                    frame_out = obj.reduce_dark(frame_out, obj.exp_t);
                case 'flat'
                    frame_out = obj.reduce_flat(frame_out);
                case 'both'
                    frame_out = obj.reduce_all(frame_out, obj.exp_t);
            end
        end
        
        %% Calibrate dark frame
        function cal_dark(obj, t, n)
            %{
            Measure the dark/background frame, the median of multiple exposures.
            The values are normalized to 1 s.
            The dark frame is saved to disk.
            
            Parameters:
            t (float): Exposure time of a single exposure [s]
            n (int): Number of exposures
            %}
            
            disp(['Dark frame calibration (`', obj.tag_use, ...
                  '`). Press ENTER when ready...']);
            pause;
            disp('Calibrating...');
            frames = zeros(obj.y_px, obj.x_px, n);
            t_old = obj.exp_t;
            obj.set_exp_t(t);
            
            for i_frame = 1: n
                frames(:, :, i_frame) = obj.get_frame('reduce', 'none', ...
                                                      'report_max', true);
            end
            
            obj.set_exp_t(t_old);
            dark_save = median(frames, 3) / t;
            obj.dark = dark_save;
            save(fullfile(obj.wd, 'cal', ...
                         [obj.tag_use, '_dark_', datestr(datetime, 30), ...
                          '.mat']), 'dark_save');
        end
        
        %% Calibrate flat field
        function cal_flat(obj, t, n)
            %{
            Measure the flat field, the median of multiple exposures. The values
            of each frames are normalized to their median.
            The flat field is saved to disk.
            
            Parameters:
            t (float): Exposure time of a single exposure [s]
            n (int): Number of exposures
            %}
            
            disp(['Flat field calibration (`', obj.tag_use, ...
                  '`). Press ENTER when ready...']);
            pause;
            disp('Calibrating...');
            frames = zeros(obj.y_px, obj.x_px, n);
            t_old = obj.exp_t;
            obj.set_exp_t(t);
            
            for i_frame = 1: n
                frame_i = obj.get_frame('reduce', 'dark', 'report_max', true);
                frames(:, :, i_frame) = frame_i / median(frame_i(:));
            end
            
            obj.set_exp_t(t_old);
            flat_save = median(frames, 3);
            obj.flat = flat_save;
            save(fullfile(obj.wd, 'cal', ...
                         [obj.tag_use, '_flat_', datestr(datetime, 30), ...
                          '.mat']), 'flat_save');
        end
        
        %% Initialize calibrations
        function cal_init(obj)
            %{
            Take calibrations or give the option to load the most current ones,
            if they exist.
            %}
            
            darks = dir(fullfile(obj.wd, 'cal', [obj.tag_use, '_dark_*.mat']));
            start_dark = false;
            
            if isempty(darks)
                start_dark = true;
            else
                answ = input(['Load the most recent dark frame ', ...
                              'calibration (`', obj.tag_use, ...
                              '`)? [Y/n]   '], 's');
                
                if strcmpi(answ, 'n')
                    start_dark = true;
                end
            end
            
            if start_dark
                obj.cal_dark(obj.dark_t, obj.cal_n);
            else
                dates = zeros(length(darks), 1);
                
                for j = 1 : length(darks)
                    dates(j) = darks(j).datenum;
                end
                
                [~, j_max] = max(dates);
                dark_save = load(fullfile(darks(j_max).folder, ...
                                          darks(j_max).name));
                obj.dark = dark_save.dark_save;
            end
            
            flats = dir(fullfile(obj.wd, 'cal', [obj.tag_use, '_flat_*.mat']));
            start_flat = false;
            
            if isempty(flats)
                start_flat = true;
            else
                answ = input(['Load the most recent flat frame ', ...
                              'calibration (`', obj.tag_use, ...
                              '`)? [Y/n]   '], 's');
                
                if strcmpi(answ, 'n')
                    start_flat = true;
                end
            end
            
            if start_flat
                obj.cal_flat(obj.flat_t, obj.cal_n);
            else
                dates = zeros(length(flats), 1);
                
                for j = 1 : length(flats)
                    dates(j) = flats(j).datenum;
                end
                
                [~, j_max] = max(dates);
                flat_save = load(fullfile(flats(j_max).folder, ...
                                          flats(j_max).name));
                obj.flat = flat_save.flat_save;
            end
        end
        
        %% Reduce with dark frame
        function frame_out = reduce_dark(obj, frame, t)
            %{
            Reduce a frame using the dark/background frame.
            
            Parameters:
            frame (arr[float]): Image
            t (float): Exposure time used for the frame [s]
            
            Output:
            frame_out (arr[float]): Reduced image
            %}
            
            frame_out = frame - (obj.dark * t);
        end
        
        %% Reduce with flat field
        function frame_out = reduce_flat(obj, frame)
            %{
            Reduce a frame using the flat field.
            
            Parameters:
            frame (arr[float]): Image
            
            Output:
            frame_out (array[float]): Reduced image
            %}
            
            frame_out = frame ./ obj.flat;
        end
        
        %% Reduce with all
        function frame_out = reduce_all(obj, frame, t)
            %{
            Reduce a frame using the dark/background frame and the flat field.
            
            Parameters:
            frame (arr[float]): Image
            t (float): Exposure time used for the frame [s]
            
            Output:
            frame_out (arr[float]): Reduced image
            %}
            
            frame_out = obj.reduce_dark(frame, t);
            frame_out = obj.reduce_flat(frame_out);
        end
    end
end
