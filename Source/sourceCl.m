%% SOURCE
% Class to simulate a calibration source for the REVOLT bench.
% Written by Paolo Turri.

%#ok<*VUNUS>

classdef (Abstract) sourceCl < handle
    
    properties (Abstract)
        lambda; % Central wavelength [m]
    end

    methods (Abstract)
        turnon(obj);
        turnoff(obj);
    end
end
