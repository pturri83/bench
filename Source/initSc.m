%% INITIALIZE
% Script to initialize MATLAB for the REVOLT bench.
% Written by Paolo Turri.

%% Debug
dbstop if error

%% Clean
clc
clear
close all force

%% Parameters
source_folder = 'C:\Paolo\Bench\Source'; % Source code folder

%% Add Path
addpath(genpath(source_folder))
