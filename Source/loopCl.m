%% Loop
% Class to manage a REVOLT loop between a WFS and a mirror.
% Written by Paolo Turri.

%#ok<*VUNUS>

classdef loopCl < handle
    
	properties
        wfs; % WFS object
        mirror; % Mirror object
        interact_cl; % CL interaction matrix
        command_cl; % CL command matrix
        eigencut_cl; % CL number of eigenmodes removed
    end
    
    methods
        %% Constructor
        function obj = loopCl(wfsObj, mirrorObj, tag, wd)
            %{
            Build object.
            
            Parameters:
            wfsObj (obj): WFS object
            mirrorObj (obj): Mirror object
            tag (str): Loop name tag
            wd (str): Working directory
            %}
            
            obj.wfs = wfsObj;
            obj.mirror = mirrorObj;
        end
        
        %% Calibrate interaction matrix
        function interact = cal_interact(obj, wfsObj, mirrorObj, tag_use)
            %{
            Calibrate an interaction matrix between a mirror and a WFS.
            
            Parameters:
            wfsObj (obj): WFS object
            mirrorObj (obj): Mirror object
            tag_use: Tag to describe the use of the detector (for saving files)
                (str)
            
            Output:
            interact (arr[float]): Interaction matrix ([slopes, actuators]) [xxxx]%%%%%
            %}
            
            interact = zeros(wfsObj.nLenslets, mirrorObj.n_act);
            
            for i_act = 1: obj.dm.n_act
                comm_tmp = mirrorObj.cmd_zero;
                comm_tmp(i_act) = obj.comm_poke;
                slopes_tmp = zeros(wfsObj.nLenslets, (2 * obj.comm_poke_n));
                
                for i_poke = 1: (2 * obj.comm_poke_n)
                    if i_poke > obj.comm_poke_n
                        comm_sign = -1;
                    else
                        comm_sign = 1;
                    end
                    
                    mirrorObj.setCmd(comm_tmp * comm_sign);
                    pause(obj.comm_poke_t);
                    wfsObj.updateFrame;
                    slopes_tmp(:, i_poke) = wfsObj.slopes * comm_sign;
                end
                
                interact(:, i_act) = mean(slopes_tmp, 2);
            end
            
            interact = interact / obj.comm_poke;
            save(fullfile(obj.wd, 'cal', ...
                          [tag_use, '_infl_', datestr(datetime, 30), ...
                           '.mat']), 'interact');
            figure('Color', 'k', 'Name', 'Interaction matrix', ...
                   'NumberTitle', 'off');
            imagesc(interact);
            xlabel('Actuator #');
            ylabel('Slope #');
            colormap hot;
            set('XColor', 'w', 'YColor', 'w', 'ZColor', 'w');
        end
        
        %% Calibrate command matrix
        function command = cal_command(obj, interact, clean_modes, tag_use)
            %{
            Calibrate a command matrix between a mirror and a WFS.
            
            Parameters:
            interact (arr[float]): Interaction matrix ([slopes, actuators]) [xxxx]%%%%%
            clean_modes (int): Number of eigenmodes to remove
            tag_use: Tag to describe the use of the detector (for saving files)
                (str)
            
            Output:
            command (arr[float]): Command matrix ([xxxxx, xxxxx]) [1/xxx]%%%%%%%%%%%%%%%%%%%
            %}
            
            [u, s, v] = svd(interact);
            eigenValues = diag(s);
            figure('Color', 'k', 'Name', 'Eigenvalues', 'NumberTitle', 'off');
            semilogy(eigenValues, 'b-');
            hold on;
            xcut = length(eigenValues) - clean_modes + 0.5;
            plot([xcut, xcut], ylim, 'r-');
            hold off;
            xlabel('Mode #');
            ylabel('Value');
            set('XColor', 'w', 'YColor', 'w', 'ZColor', 'w');
            si = diag(1 / eigenValues(1: (end - clean_modes)));
            command = v * si * u';
            save(fullfile(obj.wd, 'cal', ...
                          [tag_use, '_command_', datestr(datetime, 30), ...
                           '.mat']), 'command', 'clean_modes');
        end
        
        %% Calibrate interaction/command matrices
        function command = cal_int_comm(obj, wfsObj, mirrorObj, clean_modes, ...
                                        tag_use)
            %{
            Calibrate an interaction/command matrix pair between a mirror and a
            WFS.
            
            Parameters:
            wfsObj (obj): WFS object
            mirrorObj (obj): Mirror object
            clean_modes (int): Number of eigenmodes to remove
            tag_use (str): Tag to describe the use of the detector (for saving
                files)
            
            Output:
            command (arr[float]): Command matrix ([xxxxx, xxxxx]) [1/xxx]%%%%%%%%%%%%%%%%%%%
            %}
            
            interact = obj.cal_interact(obj, wfsObj, mirrorObj, tag_use);
            command = obj.cal_command(interact, clean_modes, tag_use);
        end
        
        %% Load command matrix
        function command = load_command(obj, tag_use)
            %{
            Load the most recent command matrix between a mirror and a WFS.
            
            Parameters:
            tag_use: Tag to describe the use of the detector (for saving files)
                (str)
            
            Output:
            command (arr[float]): Command matrix ([xxxxx, xxxxx]) [1/xxx]%%%%%%%%%%%%%%%%%%%
            %}
            
            commands = dir(fullfile(obj.wd, 'cal', [tag_use, ...
                                                    '_command_*.mat']));
            dates = zeros(length(commands));
            
            for j = 1 : length(commands)
                dates(j) = commands(j).datenum;
            end
            
            [~, j_max] = max(dates);
            command = load(fullfile(commands(j_max).folder, ...
                                    commands(j_max).name), 'command'); %%%check if the 'load' is done correctly
        end
    end
end
