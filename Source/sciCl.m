%% SCIENCE CAMERA
% Class to manage the REVOLT science camera.
% Written by Paolo Turri.%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%we need to do flat fielding and dark frames

%#ok<*VUNUS>

classdef sciCl < handle
    
	properties
        detect_tag = 'goldeye'; % Science detector tag ('zyla', 'goldeye')
        cal_n = 10; % Number of exposures for the dark frame and flat field
            % calibrations
        dark_t = 1; % Exposure time of individual exposures for the darkframe
            % calibration [s]%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        flat_t = 0.1; % Exposure time of individual exposures for the flat field
            % calibration [s]%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        tTimer = 0.5; % Plot timer interval [s]
        updatePlot = true; % Update science plot
        detect; % Detector object
        frame; % Last frame
        timerFrames; % Timer for taking frames
        figs; % Handle for science
    end
    
    properties (Constant)
        handle = 1 % Science detector handle
    end
    
    methods
        %% Constructor
        function obj = sciCl(wd)
            %{
            Build object.
            
            Parameters:
            wd (str): Working directory
            %}
            
            switch obj.detect_tag
                case 'zyla'
                    obj.detect = zylaCl(obj.handle, obj.cal_n, obj.dark_t, ...
                                        obj.flat_t, wd, 'science');
                case 'goldeye'
                    obj.detect = goldeyeCl(obj.handle, obj.cal_n, ...
                                           obj.dark_t, obj.flat_t, wd, ...
                                           'science');
            end
            
            obj.createSciencePlot;
            obj.setTimer;
        end
        
        %% Destructor
        function delete(obj)
            %{
            Delete object.
            %}
            
            obj.detect.delete;
            obj.stopTimer;
            delete(obj.timerFrames);
            set(obj.figs.fig, 'HandleVisibility', 'on');
            close(obj.figs.fig);
        end
        
        %% Frame grabber
        function frameGrabber(obj)
            %{
            Take an image and store it.
            %}
            
            obj.frame = obj.detect.get_frame;
        end
        
        %% Set timer
        function setTimer(obj)
            %{
            Define the timer.
            %}
            
            obj.timerFrames = timer('TimerFcn', @obj.updateFrame, ...
                                    'Period', obj.tTimer, ...
                                    'ExecutionMode', 'FixedSpacing');
        end
        
        %% Start timer
        function startTimer(obj)
            %{
            Start the timer.
            %}
            
            start(obj.timerFrames);
        end
        
        %% Stop timer
        function stopTimer(obj)
            %{
            Stop the timer.
            %}
            
            stop(obj.timerFrames);
        end
        
        %% Update frame
        function updateFrame(obj, varargin)
            %{
            Update the frame and plots.
            %}
            
            obj.frameGrabber;
            
            if obj.updatePlots
                obj.updateScience;
            end
        end
        
        %% Create science plot
        function createSciencePlot(obj)
            %{
            Create the window that shows the science.
            %}
            
            obj.figs.fig = figure('Color', 'k', 'Name', 'Science', ...
                                  'NumberTitle', 'off');
            obj.figs.plot = imagesc(obj.frame);
            obj.figs.axis = gca;
            %obj.figs.texts = title('','Parent', obj.figs.axis, 'Color', 'w');
            axis equal tight;
            colormap hot;
            cbar = colorbar;
            cbar.Color = 'w';
%             ticks = (obj.nPx * (1: (obj.nLenslet - 1))) + 0.5;
%             set(obj.figs.axis, 'XTick', ticks, 'YTick', ticks, ...
%                 'XColor', 'w', 'YColor', 'w', 'XTickLabel', '', ...
%                 'YTickLabel', '', 'ZColor', 'w');
%             grid
            set(obj.figs.fig,'HandleVisibility', 'callback');
        end
        
        %% Update science plot
        function updateScience(obj)
            %{
            Update the science plot.
            %}
            
            if ~isprop(obj, 'figs')
                obj.createSciencePlot;
            end
            
            maxVal = max(obj.frame(:));
            titleSpots = sprintf('Max = %d', maxVal);%%%%%%%%%%%%%%add units
            set(obj.figs.plot, 'CData', obj.frame);
            set(obj.figs.texts,'String', titleSpots);
            drawnow limitrate;
        end
    end
end
