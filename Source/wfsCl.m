%% WFS
% Class to manage a REVOLT WFS.
% Written by Paolo Turri.

%#ok<*VUNUS>

classdef wfsCl < handle
    
	properties
        lensFill = 0.75; % Valid lenslet filling
        telemFlag = true; % Record telemetry
        updatePlots = true; % Update spots and Zernikes plots
        tTimer = 0.5; % Plot timer interval [s]
        zMax = 36; % Number of Zernike modes plotted
        cal_n = 10; % Number of exposures for the dark frame and flat field
            % calibrations
        cl_tag = 'CLWFS'; % CLWFS name tag
        cl_nPx = 84; % CLWFS number of pixels in diameter per lenslet%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        cl_scaleZ = 1; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        cl_dark_t = 10; % CLWFS exposure time of individual exposures for the
            % darkframe calibration [s]
        cl_flat_t = 0.1; % CLWFS exposure time of individual exposures for the
            % flat field calibration [s]
        cl_top_crop = 400; % CLWFS top pixel cropped
        cl_left_crop = 430; % CLWFS left pixel cropped
        ol_tag = 'OLWFS'; % OLWFS name tag
        ol_nPx = 88; % CLWFS number of pixels in diameter per lenslet%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        ol_scaleZ = 1;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        ol_dark_t = 1; % OLWFS exposure time of individual exposures for the
            % dark frame calibration [s]%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        ol_flat_t = 0.1; % OLWFS exposure time of individual exposures for the
            % flat field calibration [s]%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        ol_top_crop = 380; % OLWFS top pixel cropped
        ol_left_crop = 400; % OLWFS left pixel cropped
        
        detect_tag; % WFS detector tag ('zyla', 'goldeye')
        tag; % WFS name tag
        tag_use; % Tag to describe the use of the detector (for saving files)
        handle; % WFS detector handle
        nLenslet; % Number of lenslets on the side
        nLenslets; % Total number of lenslets
        nPx; % Number of pixels per lenslet
        nPx_all;  % Number of pixels
        scaleZ; % microns on the WF per pixel%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        dark_t; % Exposure time of individual exposures for the dark frame
            % calibration [s]
        flat_t; % Exposure time of individual exposures for the flat field
            % calibration [s]
        top_crop; % Top pixel cropped
        left_crop; % Left pixel cropped
        detect; % Detector object
        wfs; % OOMAO WFS object
        z; % Zernike object
        q; %%%%%%%%%%%%%%%%%%%%what is this?
        frame; % Last frame
        slopes; % Last slopes
        telem; % Telemetry array
        timerFrames; % Timer for taking frames
        figs; % Handle for spots figure
        figz; % Handle for Zernikes figure
%         sZ4; %slope vector for unity focus error%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         dataDir; % to save registration data
%         temperature = NaN;
%         tempFlag;
%         data;
%         desiredSlopes;
    end
    
    properties (Constant)
        cl_detect_tag = 'zyla'; % CLWFS detector tag ('zyla', 'goldeye')
        ol_detect_tag = 'zyla'; % OLWFS detector tag ('zyla', 'goldeye')
        cl_handle = 0; % CLWFS detector handle
        ol_handle = 0; % OLWFS detector handle %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%find the value
        cl_nLenslet = 16; % CLWFS number of lenslets in diameter%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        ol_nLenslet = 16; % OLWFS number of lenslets in diameter%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    end
    
    methods
        %% Constructor
        function obj = wfsCl(closeOpen, telObj, wd)
            %{
            Build object.
            
            Parameters:
            closeOpen (str): Close or open WFS ('cl', 'ol')
            telObj (obj): Telescope object
            wd (str): Working directory
            %}
            
            switch closeOpen
                case 'cl'
                    obj.detect_tag = obj.cl_detect_tag;
                    obj.tag = obj.cl_tag;
                    obj.handle = obj.cl_handle;
                    obj.nLenslet = obj.cl_nLenslet;
                    obj.nPx = obj.cl_nPx;
                    obj.scaleZ = obj.cl_scaleZ;
                    obj.dark_t = obj.cl_dark_t;
                    obj.flat_t = obj.cl_flat_t;
                    obj.top_crop = obj.cl_top_crop;
                    obj.left_crop = obj.cl_left_crop;
                case 'ol'
                    obj.detect_tag = obj.ol_detect_tag;
                    obj.tag = obj.ol_tag;
                    obj.handle = obj.ol_handle;
                    obj.nLenslet = obj.ol_nLenslet;
                    obj.nPx = obj.ol_nPx;
                    obj.scaleZ = obj.ol_scaleZ;
                    obj.dark_t = obj.ol_dark_t;
                    obj.flat_t = obj.ol_flat_t;
                    obj.top_crop = obj.ol_top_crop;
                    obj.left_crop = obj.ol_left_crop;
            end
            
            obj.tag_use = [closeOpen, 'wfs'];
            
            switch obj.detect_tag
                case 'zyla'
                    obj.detect = zylaCl(obj.handle, obj.cal_n, obj.dark_t, ...
                                        obj.flat_t, wd, obj.tag_use);
                case 'goldeye'
                    obj.detect = goldeyeCl(obj.handle, obj.cal_n, ...
                                           obj.dark_t, obj.flat_t, wd, ...
                                           obj.tag_use);
            end
            
            disp(['WFS calibration (`', obj.tag_use, ...
                  '`). Press ENTER when ready...']);
            pause;
            disp('Calibrating...');
            obj.nPx_all = obj.nLenslet * obj.nPx;
            obj.wfs = shackHartmann(obj.nLenslet, obj.nPx_all, obj.lensFill);
            obj.nLenslets = obj.wfs.nValidLenslet;
            obj.wfs.camera.frameGrabber = @obj.frameGrabber;
%             obj.wfs.camera.roiSouthWestCorner = [1 1]; % the image is already crop by the framegrabber%%%probably not needed
            obj.wfs.framePixelThreshold = obj.detect.ron; %%%obj.wfs.framePixelThreshold = 100;

%             [sx sy]=meshgrid(linspace(-1,1,obj.nLenslet));
% %                 obj.wfs.validLenslet = utilities.piston(obj.nLenslet); % define circular mask on lenslets
%                 obj.wfs.validLenslet = utilities.piston(15.5,16); % define circular mask on lenslets
%                 % utilities.piston(15.7,16) Example to define less valid lenslet
%                 mask = obj.wfs.validLenslet;              
%                 obj.nValidLenslet = sum(mask(:));
%                 s = [sx(mask>0) ; sy(mask>0)];
%                 % we should inverse s, but we don't want to give too much weight to central
%                 % subaps, so we simply set all weights to 1 or -1:
%                 obj.sZ4 = sign(s);
%                 
%                 % Compute Zernike coefficients (needs a circular pupil)
%                 %obj.scaleZ = src.wavelengthInMicron/obj.wfs.lenslets.nyquistSampling/2*obj.nLenslet; % correct scale factor for PV Zernike
                
            obj.wfs.camera.frameListener.Enabled = false;
            obj.wfs.slopesListener.Enabled = false;
            obj.wfs.intensityListener = false;
            src = source;
            tel = telescope(telObj.d, ...
                            'obstructionRatio', telObj.obstructionRatio, ...
                            'resolution', obj.nPx_all, 'samplingTime', 0.01, ...
                            'fieldOfViewInArcMin', 10);
            src = src .* tel * obj.wfs; %#ok<NASGU>
            obj.wfs.INIT;
            +obj.wfs;
            obj.z = zernike((1: obj.zMax), 'resolution', obj.nPx_all);
            obj.z = obj.z\+obj.wfs;%%%%%%%%%%%%%%%%%%%%format it correctly
            obj.q = sqrt((2 - (obj.z.m == 0)) .* (obj.z.n + 1));%%%%%%%%%%%%%%%%what is this?
            obj.telem = struct('time', [], 'intens', [], 'slope', [], ...
                               'zern', []);
            obj.createSpotPlot;
            obj.createZernPlot;
            obj.setTimer;
        end
        
        %% Destructor
        function delete(obj)
            %{
            Delete object.
            %}
            
            obj.wfs.delete;
            obj.detect.delete;
            obj.stopTimer;
            delete(obj.timerFrames);
            set(obj.figs.fig, 'HandleVisibility', 'on');
            set(obj.figz.fig, 'HandleVisibility', 'on');
            close(obj.figs.fig);
            close(obj.figz.fig);
        end
        
        %% Frame grabber
        function fram = frameGrabber(obj)
            %{
            Take an image. The frame is cropped.
            
            Output:
            frame (arr[float]): Image
            %}
            
            %ima = reshape(typecast(obj.cam.getFrame(1),'uint16'),128,128);
            fullframe = obj.detect.get_frame('reduce', 'both');
            fram = obj.cropFrame(fullframe);
            %frame = double(fullframe(397:1804, 397:1804));
%                 out = double(frame(567:1974, 367:1774));
%                 out = double((reshape(frames(1).image,frames(1).height,frames(1).width)));
%                 out = double(reshape(typecast(frames(1).image,'uint16'),120,120));
            %out = obj.smartCropFrame(double(ima));
            obj.frame = fram;
        end
        
        %% Set timer
        function setTimer(obj)
            %{
            Define the timer.
            %}
            
            obj.timerFrames = timer('TimerFcn', @obj.updateFrame, ...
                                    'Period', obj.tTimer, ...
                                    'ExecutionMode', 'FixedSpacing');
        end
        
        %% Start timer
        function startTimer(obj)
            %{
            Start the timer.
            %}
            
            start(obj.timerFrames);
        end
        
        %% Stop timer
        function stopTimer(obj)
            %{
            Stop the timer.
            %}
            
            stop(obj.timerFrames);
        end
        
        %% Update frame
        function updateFrame(obj, varargin)%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%try removing varargin. if it works, remove it also from the scienceclass
            %{
            Update the frame, telemetry, and plots.
            %}
            
            time_now = now;
            +obj.wfs;
            obj.z=obj.z\+obj.wfs;%%%format it correctly
            obj.slopes = obj.wfs.slopes;
            
            if obj.telemFlag
                obj.telem.time = [obj.telem.time; time_now];
                obj.telem.intens = [obj.telem.intens; obj.wfs.lensletIntensity];
                obj.telem.slope = [obj.telem.slope; obj.wfs.slopes];
                obj.telem.zern = [obj.telem.zern; obj.z];
            end
            
            if obj.updatePlots
                obj.updateSpot;
                obj.updateZernike;
            end
        end
        
        %% Create spots plot
        function createSpotPlot(obj)
            %{
            Create the window that shows the spots and slopes.
            %}
            
            [row, col] = find(obj.wfs.validLenslet);
            px = (col - 1) * obj.nPx;
            py = (row - 1) * obj.nPx;
            px = px + obj.wfs.referenceSlopes(1: (end / 2),1);
            py = py + obj.wfs.referenceSlopes(((end / 2) + 1): end);
            sx = zeros(length(px), 1);
            sy = zeros(length(py), 1);
            obj.figs.fig = figure('Color', 'k', ...
                                  'Name', sprintf('%s spots', obj.tag), ...
                                  'NumberTitle', 'off');
            obj.figs.plot = imagesc(obj.wfs.camera.frame);
            hold on;
            obj.figs.points = plot(px, py, 'g+', 'markersize', 8);
            obj.figs.quiver = quiver(px, py, sx, sy); %%%do tests to see if the position of the lenslets is correct
            hold off;
            obj.figs.axis = gca;
            %obj.figs.texts = title('','Parent', obj.figs.axis, 'Color', 'w');
            axis equal tight;
            colormap hot;
            cbar = colorbar;
            cbar.Color = 'w';
            ticks = (obj.nPx * (1: (obj.nLenslet - 1))) + 0.5;
            set(obj.figs.axis, 'XTick', ticks, 'YTick', ticks, ...
                'XColor', 'w', 'YColor', 'w', 'ZColor', 'w', ...
                'XTickLabel', '', 'YTickLabel', '');
            grid;
            set(obj.figs.fig, 'HandleVisibility', 'callback');
        end
        
        %% Create Zernike plot
        function createZernPlot(obj)
            %{
            Create the window that shows the Zernike modes.
            %}
            
            obj.figz.fig = figure('Color', 'k', ...
                                  'Name', sprintf('%s Zernikes', obj.tag), ...
                                  'NumberTitle', 'off');
            obj.figz.plot = stem(((1: length(obj.z.c)) + 1), ...
                                 (obj.scaleZ * obj.z.c .* transpose(obj.q)), ...
                                 'yo', 'LineWidth', 2);
            obj.figs.axis = gca;
            colormap hot;
            cbar = colorbar;
            cbar.Color = 'w';
            ticks = (1: length(obj.z.c)) + 1;
            set(obj.figs.axis, 'XTick', ticks, 'Color', 'k', 'XColor', 'w', ...
                'YColor', 'w');
%             grid
%             obj.z=obj.z\obj.wfs;
%             set(obj.figz.plot,'Ydata',obj.scaleZ*obj.z.c.*transpose(obj.q));%%%for some reason, they are re-plotting the zern coefficients, after "scaling" (?) them
%            xlim([2 22])
            xlabel('Zernike mode');
            ylabel('WFE P-V (\mum)');
            set(obj.figz.fig, 'HandleVisibility', 'callback');
        end
        
        %% Update spots plot
        function updateSpot(obj)
            %{
            Update the spots and slopes plot.
            %}
            
            if ~isprop(obj, 'figs')
                obj.createSpotPlot;
            end
            
            sx = obj.wfs.slopes(((end / 2) + 1): end);
            sy = obj.wfs.slopes(1: (end / 2),1);
            tip = mean(sx(obj.wfs.lensletIntensity > 0)) - (0 * (obj.nPx - 1) / 2);%%%%choose the intensity threshold%%%%why is there a zero?
            tilt = mean(sy(obj.wfs.lensletIntensity > 0)) - (0 * (obj.nPx - 1) / 2);
            % Compute focus
%                 obj.z=obj.z\obj.wfs;
%                 focus = obj.scaleZ*obj.z.c(3)*obj.q(3);
            focus = sum(obj.wfs.slopes .* obj.sZ4) / 2 / sum(obj.wfs.validLenslet(:));
            maxVal = max(obj.wfs.camera.frame(:));
            titleSpots = sprintf('Tip = %3.2f, Tilt = %3.2f, Focus = %3.2f, Max = %d',...
                tip, tilt, focus, maxVal);%%%%%%%%%%%%%%%%%%%add units
            set(obj.figs.plot, 'CData', obj.frame);
            set(obj.figs.quiver, 'udata', sx, 'vdata', sy)%%%do tests to see if the direction of the arrows is correct
            set(obj.figs.texts, 'String', titleSpots);
            drawnow limitrate;
        end
        
        %% Update Zernike plot
        function updateZernike(obj)
            %{
            Update the Zernike modes plot.
            %}
            
            if ~isprop(obj, 'figz')
                obj.createZernPlot;
            end
            
            set(obj.figz.plot, 'Ydata', (obj.scaleZ * obj.z.c .* transpose(obj.q)));%%%%hy this multiplication?
            drawnow limitrate;
        end
        
        %% Crop the frame
        function frame = cropFrame(obj, fullframe)
            %{
            Crop the full frame to the size needed by the OOMAO WFS object.
            
            Parameters:
            fullframe (arr[float]): Image
            
            Output:
            frame (arr[float]): Cropped image
            %}
            
            frame = fullframe(obj.top_crop: ...
                              (obj.top_crop + obj.nPx_all - 1), ...
                              obj.left_crop: ...
                              (obj.left_crop + obj.nPx_all - 1));
        end
    end

%         
%         
%         %% CroppedRefSlopes
%         function out = cropedRefSlopes(obj)
%             validLenslet = obj.wfs.validLenslet;
%             % Construct the reference slopes for the smartly croped frame
%             spotPitchInRawPixel = linspace(8.25,8.25+12.5*9,10);
%             %subapPitchInRawPixel = [2 15 15+12 40 52 65 65+12 90 102 115]+6.5;
%             subapPitchInRawPixel = [2 2+12 27 27+12 52 52+12 77 77+12 102 102+12]+6.5;
%             
%             shift = repmat(spotPitchInRawPixel-subapPitchInRawPixel,10,1);
%             xRefSlope2D= ones(10)*5.5 + shift; % for full lenslet array
%             yRefSlope2D= ones(10)*5.5 + shift';
%             
%             % You still have to raster the refslope according to the validlenslet mask:
%             
%             out = [xRefSlope2D(validLenslet>0) ; yRefSlope2D(validLenslet>0)];
%         end
%         
%         %% Subap corner pixels
%         function [validCornerX validCornerY] =subapCornerPixels(obj)
%             validLenslet = obj.wfs.validLenslet;
%             subapPitchInRawPixel = [2 2+12 27 27+12 52 52+12 77 77+12 102 102+12]+6.5;
%             cornerSubap = subapPitchInRawPixel -6.5 +1;
%             [cornerX cornerY] = meshgrid(cornerSubap,cornerSubap);
%             validCornerX = cornerX(validLenslet>0);
%             validCornerY = cornerY(validLenslet>0);
%         end
    

end
