%% DM
% Class to manage an ALPAO DM.
% Written by Paolo Turri.

%#ok<*VUNUS>

classdef dmCl < mirrorCl
    
    properties
        tag = 'DM'; % Mirror name tag
        figc_size = [1, 0, 400, 400, 400]; % Actuators figure size [px]
            % [monitor #, left, bottom, width, height]
        n_act = 277; % Total numer of actuators
        n_act_d = 19; % Numer of actuators on the diameter
        n_act_d_p = 17; % Numer of actuators on the pupil diameter
        handle = 'alpaoHSDM277-15-005' % DM handle
        folder_mask = 'C:\Paolo\Bench\Drivers\Alpao DM'; % Folder containing the
            % actuators mask
        file_mask = 'mask_dm277-15.mat'; % File containing the actuators mask
        
        sdkid; % DM handle for the Alpao SDK
        pitch; % Pitch on the telescope aperture [m]
        infunc; % Influence functions
        mask; % Actuators mask
        cmd; % Last command applied
        cmd_zero; % Zero command
        cmd2d; % 2D command array
        figc; % Handle for actuators figure
    end
    
    methods
        %% Constructor
        function obj = dmCl(telObj)
            %{
            Build object.
            
            Parameters:
            telObj (obj): Telescope object
            %}
            
            load(fullfile(obj.folder_mask, obj.file_mask), 'mask')
            obj.mask = mask;
            obj.sdkid = asdkMex(0, obj.handle);
            obj.pitch = telObj.d / (obj.n_act_d_p - 1);
            obj.cmd = zeros(obj.n_act, 1);
            obj.cmd_zero = zeros(obj.n_act, 1);
            obj.cmd2d = NaN(obj.n_act_d);
            
            if obj.updatePlots
                obj.createActPlot;
            end
            
            obj.reset;
        end
        
        %% Destructor
        function delete(obj)
            %{
            Delete object.
            %}
            
            asdkMex(1, obj.sdkid);
            
            if ~isempty(obj.figc.fig)
                close(obj.figc.fig);
            end
        end
        
        %% Apply command
        function setCmd(obj, cmd)
            %{
            Apply a command to the DM.
            
            Parameters:
            cmd (arr[float]): Command
            %}
            
            obj.cmd = cmd;
            obj.cmd2d(obj.mask) = cmd;
            asdkMex(2, obj.sdkid, cmd);
            
            if obj.updatePlots
                obj.updateAct;
            end
            
            pause(obj.settle_t);
        end
        
        %% Reset DM
        function reset(obj)
            %{
            Reset the DM.
            %}
            
            asdkMex(7, obj.sdkid);
            obj.setCmd(obj.cmd_zero);
        end
    end
end