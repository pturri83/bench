%% SLM
% Class to manage a Hamamatsu's Goldeye G-008 Cool TEC1 detector.
% Written originally by Uriel Conod. Modified by Paolo Turri.

%#ok<*VUNUS>

classdef slmCl < mirrorCl
    
    properties
        tag = 'SLM'; % Mirror name tag
        figc_size = [1, 1, 401, 400, 400]; % Virtual actuators figure size [px]
            % [monitor #, left, bottom, width, height]
        n_act_d = 17; % Numer of virtual actuators on the pupil diameter%%%%%%%%%%%%%%%%
        x_off = 0; % Pupil offset in x [px]
        y_off = 0; % Pupil offset in y [px]
        t_int_wl = 10; % P-V internal tip applied (in number of wavelengths over
            % the pupil)
        t_ext_wl = 10; % P-V external tilt applied (in number of wavelengths
            % over the SLM height)
        d_pupil = 364; % Pupil diameter [px]
        
        alpha; % 8-bit value for 2pi modulation
        screen; % Screen figure
        pad; % Padding (y, x) to be added on each side of the pupil [px]
        pupil; % Pupil mask
        arr_pre; % Last screen values before being sent to the SLM figure
        arr; % Last screen values sent to the SLM figure
        zero_arr; % Zero screen values
        t_int; % Internal tip (horizontal) array, not wrapped
        t_ext; % External tilt (vertical) array, wrapped
        dm; % OOMAO DM object
        tel; % OOMAO telescope object
        pitch; % Virtual pitch on the telescope aperture [m]
        infunc; % Virtual influence functions
        mask; % Virtual actuators mask
        cmd; % Last virtual command applied
        cmd_zero; % Virtual zero command
        cmd2d; % Virtual 2D command array
        figc; % Handle for actuators figure
        figs; % Handle for SLM figure
    end
    
    properties (Constant)
        x_px = 792; % Width of the screen [px]
        y_px = 600; % Height of the screen [px]
        folder_alpha = 'C:\Paolo\Bench\Drivers\Hamamatsu SLM'; % Folder
            % containing the alpha values
        file_alpha = 'alpha_slm_lsh0702233.mat'; % File containing the alpha
            % values
    end
    
    methods
        %% Constructor
        function obj = slmCl(sourceObj, telObj)
            %{
            Build object.
            
            Parameters:
            sourceObj (obj): Source object
            telObj (obj): Telescope object
            %}
            
            load(fullfile(obj.folder_alpha, obj.file_alpha), 'alpha_tab');
            alpha_tab(:, 1) = alpha_tab(:, 1) * 1e-9;
            wl_diff = abs(alpha_tab(:, 1) - sourceObj.lambda);
            [~, sort_idx] = sort(wl_diff);
            obj.alpha = uint8((((sourceObj.lambda - ...
                                alpha_tab(sort_idx(1), 1))) * ...
                               ((alpha_tab(sort_idx(2), 2) - ...
                                alpha_tab(sort_idx(1), 2))) / ...
                               ((alpha_tab(sort_idx(2), 1) - ...
                                alpha_tab(sort_idx(1), 1)))) + ...
                              alpha_tab(sort_idx(1), 2));
            obj.n_act_d_p = obj.n_act_d;%%%%%does oomao put the edge actuators exacly on the pupil edge? if not, change this equation
            obj.pitch = telObj.d / (obj.n_act_d_p - 1);
            bif = gaussianInfluenceFunction(obj.cross_couple, obj.pitch);
            obj.dm = deformableMirror(obj.n_act_d, 'modes', bif, ...
                                      'resolution', obj.d_pupil, ...
                                      'validActuator', ...
                                      logical(utilities.piston(obj.n_act_d)));
            obj.n_act = obj.dm.nValidActuator;
            obj.mask = logical(obj.dm.validActuator);
            obj.pad = abs(obj.d_pupil - [obj.y_px, obj.x_px]) / 2;
            obj.zero_arr = zeros(obj.y_px, obj.x_px);
            obj.cmd = zeros(obj.n_act, 1);
            obj.cmd_zero = zeros(obj.n_act, 1);
            obj.cmd2d = NaN(obj.n_act_d);
            obj.tel = telescope(telObj.d, ...
                                'obstructionRatio', telObj.obstructionRatio, ...
                                'resolution', obj.d_pupil, ...
                                'samplingTime', 0.01, ...
                                'fieldOfViewInArcMin', 10);
            obj.pupil = obj.tel.pupil;
            obj.pupil = padarray(obj.pupil, obj.pad, 0, 'both');
            [x_mesh, y_mesh] = meshgrid(1: obj.x_px, 1: obj.y_px);
            obj.t_int =  x_mesh * obj.t_int_wl * 2 * pi / obj.d_pupil;
            obj.t_ext =  mod((y_mesh * obj.t_ext_wl * 2 * pi / obj.y_px), ...
                             obj.alpha);
            obj.create_infunc;
            obj.createScreen;
            
            if obj.updatePlots
                obj.createSlmPlot;
                obj.createActPlot;
            end
            
            obj.reset;
        end
        
        %% Destructor
        function delete(obj)
            %{
            Delete object.
            %}
            
            delete(obj.dm);
            delete(obj.tel);
            close(obj.screen.fig);
            close(obj.figs.fig);
        end
        
        %% Create virtual influence functions
        function create_infunc(obj)
            %{
            Create the influence functions that mimic the actuators on a DM.
            %}
            
            obj.infunc = zeros(obj.y_px, obj.x_px, obj.n_act);
            
            for k = 1: obj.n_act
                obj.dm.coefs(k) = 1;
                obj.infunc(:, :, k) = ...
                    uint8(padarray((obj.dm.surface .* obj.tel.pupil), ...
                                   obj.pad, NaN,'both') .* obj.pupil);
                obj.dm.coefs(k) = 0;
            end
            
            obj.infunc = obj.alpha * mat2gray(obj.infunc);
        end
        
        %% Create screen
        function createScreen(obj)
            %{
            Create the screen that holds the SLM.
            %}
            
            g_root = groot;
            screen_coor = g_root.MonitorPositions;
            obj.screen.fig = figure('position', ...
                                    [screen_coor(1), screen_coor(3), ...%%%%%%%%%%%%%%check the coordinates of the SLM screen
                                     obj.x_px, obj.y_px], 'menubar', 'none', ...
                                    'toolbar', 'none', 'resize', 'off', ...
                                    'Color', 'k');
            obj.screen.axis = axes('parent', obj.screen.fig);
            obj.screen.plot = imagesc(obj.zero);
            axis off;
            set(obj.screen.axis, 'position', [0, 0, 1, 1], 'visible', 'off');
            colormap('gray'); 
        end
        
        %% Set shape
        function setShape(obj, shape)
            %{
            Change the shape on the SLM.
            1) Apply internal tip
            2) Wrap phase
            3) Offset center
            4) Apply external tilt
            5) Send to SLM
            
            Parameters:
            shape (arr[float]): Wavefront
            %}
            
            obj.arr_pre = shape;
            shape(~obj.pupil) = 0;
            shape = shape + obj.t_int;
            shape = mod(shape, obj.alpha);
            shape = circshift(shape, [obj.y_off, obj.x_off]);
            shape(~obj.pupil) = obj.t_int(~obj.pupil);
            obj.arr = shape;
            set(obj.screen.plot, 'CData', uint8(shape));
            
            if obj.updatePlots
                obj.updateSlm;
                obj.updateAct;
            end
            
            pause(obj.settle_t);
        end
        
        %% Reset SLM
        function reset(obj)
            %{
            Reset the SLM.
            %}
            
            obj.setShape(obj.zeros);
        end
        
        %% Apply virtual command
        function setCmd(obj, cmd)
            %{
            Apply a virtual command to the SLM.
            
            Parameters:
            cmd (arr[float]): Virtual command
            %}
            
            obj.cmd = cmd;
            obj.cmd2d(obj.mask) = cmd;
            cmd_img = zeros(size(obj.pupil));
            
            for i_act = 1 : obj.n_act
                cmd_act = obj.infunc(:, :, i_act) * cmd(i_act);
                cmd_img = cmd_img + cmd_act;
            end
            
            obj.setShape(cmd_img);
        end
        
        %% Create SLM plot
        function createSlmPlot(obj)
            %{
            Create the window that shows the SLM.
            %}
            
            obj.figs.fig = figure('Color', 'k', 'Name', 'SLM', ...
                                  'NumberTitle', 'off');
            obj.figs.fig = benchCl.newFig(obj.figs_size, 'SLM screen');
            tiledlayout(2,1);
            obj.figs.ax1 = nexttile;
            obj.figs.plot = imagesc(obj.figs.ax1, obj.zero);
            axis equal tight;
            colormap hot;
            cbar1 = colorbar;
            cbar1.Color = 'w';
            set(obj.figs.ax1, 'XColor', 'w', 'YColor', 'w', 'ZColor', 'w');
            obj.figs.ax2 = nexttile;
            obj.figs.plot = imagesc(obj.figs.ax2, obj.zero);
            axis equal tight;
            colormap hot;
            cbar2 = colorbar;
            cbar2.Color = 'w';
            set(obj.figs.ax2, 'XColor', 'w', 'YColor', 'w', 'ZColor', 'w');
            set(obj.figs.fig, 'HandleVisibility', 'callback');
        end
        
        %% Update SLM plot
        function updateSlm(obj)
            %{
            Update the SLM figure.
            %}
            
            if ~isprop(obj, 'figs')
                obj.createSlmPlot;
            end
            
            axes(obj.figs.ax1);
            set(obj.figs.plot, 'CData', obj.arr_pre);
            axes(obj.figs.ax2);
            set(obj.figs.plot, 'CData', obj.arr);
            drawnow limitrate;
        end
    end
end