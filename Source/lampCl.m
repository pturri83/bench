%% LAMP
% Class to simulate the white light lamp for the REVOLT bench.
% Written by Paolo Turri.

%#ok<*VUNUS>

classdef lampCl < sourceCl
    
    properties
        lambda = 660e-9; % Central wavelength [m] %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        port = 'COM3'; % Serial port
        baud = 9600; % Serial port baud rate
        parity = 'none'; % Serial port parity check
        stopbit = 1; % Serial port stop bits
        term = 'CR'; % Serial port command terminator
        powermax = 1; % Maximum power [W???]%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%it should be 10% of the maximum power rating of the bulb
        power = 50; % Power setting (percentage of the maximum power)%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        serport; % Serial port object
        staus; % Status (on or off)
    end
    
    methods
        %% Constructor
        function obj = lampCl
            %{
            Build object.
            %}
            obj.serport = serialport(obj.port, obj.baud, ...
                                     'Parity', obj.parity, ...
                                     'StopBits', obj.stopbit, ...
                                     'Terminator', obj.term);
            fopen(obj.serport);
            obj.turnoff;
            fprintf(obj.serport, 'MODE=0');
            
            fprintf(obj.serport, 'P-LIM?');%%%%%%%%%%%%%delete this section
            out = fscanf(obj.serport);
            disp('vvv')
            disp(out)
            fprintf(obj.serport, 'P-PRESET?');
            out = fscanf(obj.serport);
            disp('mmm')
            disp(out)
            
            fprintf(obj.serport, sprintf('P-LIM=%u', obj.powermax));
            obj.set_power(obj.power);
        end
        
        %% Destructor
        function delete(obj)
            %{
            Delete object.
            %}
            
            obj.turnoff;
            fclose(obj.serport);
        end
        
        %% On
        function turnon(obj)
            %{
            Turn on the light source.
            %}
            
            obj.status = 'on';
            fprintf(obj.serport, 'START');
            pause(1);%%%%%%%%%%%%%%%%%%%check how long it takes to stabilize lamp
        end
        
        %% Off
        function turnoff(obj)
            %{
            Turn off the light source.
            %}
            
            obj.status = 'off';
            fprintf(obj.serport, 'STOP');
            pause(1);%%%%%%%%%%%%%%%%%%%check how long it takes to stabilize lamp
        end
        
        %% Set power
        function set_power(obj, pwr)
            %{
            Set the lamp power, as a percentage of the maximum wattage allowed.%%%%check if the lamp changes power if it is changed while it's ON. otherwise, we need first to turn OFF, and then turn ON again
            
            Parameters:
            pwr (float): Power (percentage of the maximum wattage)
            %}
            
            if pwr > 100
                pwr = 100;
            elseif pwr < 0
                pwr = 0;
            end
            
            obj.power = pwr;
            pwr_abs = pwr * obj.powermax / 100;
            fprintf(obj.serport, sprintf('P-PRESET=%f', pwr_abs));
        end
    end
end
