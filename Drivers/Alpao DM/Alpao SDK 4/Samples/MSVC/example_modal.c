/* Alpao SDK C Header */
#include "asdkWrapper.h"

/* System Headers */
#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>

/* Wait for human action */
void pause()
{
	printf("Press ENTER to exit... \n");
	fflush(stdin); getchar();
}

Scalar* readCsv(const char* csv_file, int max_col, int max_line) {
	Scalar* out = malloc(sizeof(Scalar) * max_col * max_line);
	FILE* fid = fopen(csv_file, "r");

	const int line_max_length = max_col * 20;
	char* line = malloc(line_max_length);


	if (fid == NULL) {
		printf("Cannot open %s.\n", csv_file);
		free(line);
		return out;
	}

	int lineId = 0;
	while (fgets(line, line_max_length, fid) != NULL && lineId < max_line) {

		int colId = 0;

		char* token = strtok(line, ";");
		while (token != NULL && colId < max_col) {

			out[lineId * max_col + colId] = strtod(token, NULL);

			token = strtok(NULL, ";\n");

			colId++;
		}

		lineId++;
	}

	free(line);

	fclose(fid);

	return out;
}

/* Example using C API */
int wrapperExample()
{
	UInt nbAct, act, idx;
	COMPL_STAT ret;
	Scalar *   data;
	Scalar     tmp;

	asdkDM * dm = NULL;

	char   serialName[128] = "";

	/* Get serial number */
	printf("Please enter the S/N within the following format: BXXYYY (see DM backside)\n");
	scanf_s("%s", serialName, sizeof(serialName));

	/* Load configuration file */
	dm = asdkInit(serialName);
	if (dm == NULL)
	{
		return -1;
	}

	/* Get the number of actuators */
	ret = asdkGet(dm, "NbOfActuator", &tmp);
	nbAct = (UInt)tmp;

	/* Check errors */
	if (ret != SUCCESS)
	{
		return -1;
	}

	printf("Number of actuators: %d\n", nbAct);

	/* Initialize data */
	data = (Scalar*)calloc(nbAct, sizeof(Scalar));
	for (idx = 0; idx < nbAct; idx++)
	{
		data[idx] = 0;
	}


	/* Read the Z2C file (each line is a mode) */
#define nb_modes 100 // Change this to the number of lines in the Z2C.csv file.

	const char* ace_path = getenv("ACECFG");

	char* fileName = malloc(strlen(ace_path) + strlen(serialName) + 10);
	sprintf(fileName, "%s\\%s-Z2C.csv", ace_path, serialName);
	Scalar* z2c = readCsv(fileName, nbAct, nb_modes);

	/* Create the modeVector */
	Scalar modeVector[nb_modes] = { 0 };
	modeVector[3] = 5;
	modeVector[4] = 5;

	/* Matrix product */
	for (int modeId = 0; modeId < nb_modes; modeId++) {
		for (int actId = 0; actId < nbAct; actId++) {
			data[actId] += z2c[modeId * nbAct + actId] * modeVector[modeId];
		}
	}

	free(z2c);

	ret = asdkSend(dm, data);
	Sleep(500);

	printf("Done.\n");

	/* Release memory */
	free(data);

	/* Reset mirror values */
	asdkReset(dm);

	/* Release */
	asdkRelease(dm);
	dm = NULL;

	return 0;
}

/* Main program */
int main(int argc, char ** argv)
{
	int ret = wrapperExample();

	/* Print last error if any */
	asdkPrintLastError();

	pause();
	return ret;
}