/* Alpao SDK C Header */
#include "asdkWrapper.h"

/* System Headers */
#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>

/* Wait for human action */
void pause()
{
    printf( "Press ENTER to exit... \n" );
    fflush( stdin ); getchar();
}

/* Example using C API */
int wrapperExample()
{
    UInt nbModes, mode, idx;
    COMPL_STAT ret;
    Scalar *   data;
    Scalar     tmp;

    asdkDM * dm = NULL;
    
	char   serialName[128] = "";
	
	/* Get serial number */
    printf( "Please enter the S/N within the following format: BXXYYY (see DM backside)\n" );
    scanf_s( "%s", serialName, sizeof(serialName) );

    /* Load configuration file */
    dm = asdkInit( serialName );
    if ( dm == NULL )
    {
        return -1;
    }
        
    /* Get the number of modes */
    ret = asdkGet( dm, "NbOfMode", &tmp );
    nbModes = (UInt) tmp;

    /* Check errors */
    if ( ret != SUCCESS )
    {
        return -1;
    }
    
    printf( "Number of modes: %d\n", nbModes );

    /* Initialize data */
    data = (Scalar*) calloc( nbModes, sizeof( Scalar ) );
    for ( idx = 0 ; idx < nbModes ; idx++ )
    {
        data[idx] = 0;
    }

    /* Send value to the DM */
    printf( "Send data on mirror (data LED should blink):\n" );
    for ( mode = 0; mode < nbModes && ret == SUCCESS; mode++ )
    {
		printf( "." );

        data[ mode ] = 1e-6; // m PV Wavefront
        ret = asdkSend( dm, data );
		Sleep( 1 ); // 1 second
        data[ mode ] = 0;
    }
	printf( "Done.\n" );

    /* Release memory */
    free( data );

    /* Reset mirror values */
    asdkReset( dm );

    /* Release */
    asdkRelease( dm );
    dm = NULL;

    return 0;
}

/* Main program */
int main( int argc, char ** argv )
{
    int ret = wrapperExample();
    
    /* Print last error if any */
    asdkPrintLastError();

    pause();
    return ret;
}
