// Alpao SDK Header: All types and class are in the ACS namespace
#include "asdkDM.h"
using namespace acs;

// System Headers
#include <iostream>
#include <windows.h>
using namespace std;

// Wait for human action
void pause()
{
    cout << endl << "Press ENTER to exit... " << endl << flush;
#undef max
    cin.clear();
    cin.ignore( std::numeric_limits <std::streamsize> ::max(), '\n' );
}

// Example using C++ API
int dmExample()
{
    String serialName;

    // Get serialName number
    cout << "Please enter the S/N within the following format: BXXYYY (see DM backside)" << endl;
    cin >> serialName;
    cin.ignore( 10, '\n' );

    // Load configuration file
    DM dm( serialName.c_str() );

    // Get the number of actuators
    UInt nbModes = (UInt) dm.Get( "NbOfMode" );

    // Check errors
    if ( !dm.Check() )
    {
        return -1;
    }
    
    cout << "Number of modes: " << nbModes << endl;

    // Initialize data
    Scalar *data = new Scalar[nbModes];
    for ( UInt i = 0 ; i < nbModes ; i++ )
    {
        data[i] = 0;
    }
    
    cout << "Send data on mirror (data LED should blink): " << endl;
    // Send value to the DM
    for ( UInt mode = 0; mode < nbModes && dm.Check(); mode++ )
    {
        cout << ".";

        data[ mode ] = 1e-6; // in m PV Wavefront
        dm.Send( data );
        Sleep( 1 ); // 1 second
        data[ mode ] = 0;
    }
    cout << "Done." << endl;
    
    // Release memory
    delete [] data;

    return 0;
}

// Main program
int main( int argc, char ** argv )
{
    int ret = dmExample();
    
    // Print last errors if any
    while ( !DM::Check() ) DM::PrintLastError();

    pause();
    return ret;
}
