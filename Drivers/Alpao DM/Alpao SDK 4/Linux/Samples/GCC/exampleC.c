/* Alpao SDK C Header */
#include "asdkWrapper.h"

/* System Headers */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/* Example using C API */
int wrapperExample()
{
    UInt nbAct, act, idx;
    COMPL_STAT ret;
    Scalar *   data;
    Scalar     tmp;

    asdkDM * dm = NULL;
    
	char   serial[128] = "";
	
	/* Get serial number from user*/
    printf( "Please enter the S/N within the following format: BXXYYY (see DM backside)\n" );
    scanf( "%s", serial );

    /* Load configuration file */
    dm = asdkInit( serial );
    if ( dm == NULL )
    {
        return -1;
    }
        
    /* Get the number of actuators */
    ret = asdkGet( dm, "NbOfActuator", &tmp );
    nbAct = (UInt) tmp;

    /* Check for errors */
    if ( ret != SUCCESS )
    {
        return -1;
    }
    
    printf( "Number of actuators: %d\n", nbAct );

    /* Initialize data */
	/* Array of values to be sent to the mirror, 
	the number of element should be equal to the number of actuator */
    data = (Scalar*) calloc( nbAct, sizeof( Scalar ) );
    for ( idx = 0 ; idx < nbAct ; idx++ )
    {
        data[idx] = 0;
    }

    /* Send value to the DM */
    printf( "Send data on mirror (data LED should blink):\n" );
    asdkReset( dm ) ;
	
    for ( act = 0; act < nbAct && ret == SUCCESS;  act++ )
    {
		printf( "." );fflush(stdout);
		/* prepare 35% stroke for actuator 'act' (value normalized between -1 and 1) */
        data[ act ] = 0.35;
		/* Send value to the mirror */
        ret = asdkSend( dm, data );
		sleep( 1 );
		/* Set value of 'act' to Zero */
        data[ act ] = 0;
    }

	printf( "Done.\n" );

    /* Release memory */
    free( data );

    /* Reset mirror values */
    asdkReset( dm );

    /* Release */
    asdkRelease( dm );
    dm = NULL;

    return 0;
}

/* Main program */
int main()
{
    int ret = wrapperExample();
    
    /* Print last error if any */
    asdkPrintLastError();

    return ret;
}
