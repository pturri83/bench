Requirement of Alpao SDK libraries
==================================

 Alpao libraries are build using:
 --------------------------------
    CentOS release 5.10 (Final)
    g++ (GCC) 4.1.2 20080704 (Red Hat 4.1.2-54)
    glibc-devel 2.5-118.el5_10.2

 Minimum requirement are:
 ------------------------
    GLIBC    - 2.2.5
    GLIBCXX  - 3.4
    GCC      - 3.0