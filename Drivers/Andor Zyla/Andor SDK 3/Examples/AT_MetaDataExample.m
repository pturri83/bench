disp('Andor SDK3 Metadata Example');

%AOI Setup
frameCount = 100;
AOIHeight = 1080;
AOIWidth = 1920;
AOITop = 1;
AOILeft = 1;

[rc] = AT_InitialiseLibrary();
AT_CheckError(rc);
[rc,hndl] = AT_Open(0);
AT_CheckError(rc);
disp('Camera initialized');
[rc] = AT_SetEnumString(hndl,'ElectronicShutteringMode','Rolling');
AT_CheckWarning(rc);
[rc] = AT_SetFloat(hndl,'ExposureTime',0.01);
AT_CheckWarning(rc);
[rc] = AT_SetEnumString(hndl,'CycleMode','Fixed');
AT_CheckWarning(rc);
[rc] = AT_SetEnumString(hndl,'TriggerMode','Internal');
AT_CheckWarning(rc);
[rc] = AT_SetEnumString(hndl,'SimplePreAmpGainControl','16-bit (low noise & high well capacity)');
AT_CheckWarning(rc);
[rc] = AT_SetInt(hndl,'FrameCount',frameCount);
AT_CheckWarning(rc);

%Enable Metadata
[rc] = AT_SetBool(hndl,'MetadataEnable',1);
AT_CheckWarning(rc);
[rc] = AT_SetBool(hndl,'MetadataTimestamp',1);
AT_CheckWarning(rc);

%Set up AOI
[rc] = AT_SetInt(hndl, 'AOIHeight',AOIHeight);
AT_CheckWarning(rc);
[rc] = AT_SetInt(hndl, 'AOIWidth',AOIWidth);
AT_CheckWarning(rc);
[rc] = AT_SetInt(hndl, 'AOITop',AOITop);
AT_CheckWarning(rc);
[rc] = AT_SetInt(hndl, 'AOILeft',AOILeft);
AT_CheckWarning(rc);

%Get Clock Frequency and Framerate
[rc,frameRate] = AT_GetFloat(hndl,'FrameRate');
AT_CheckWarning(rc);
[rc,clockFreq] = AT_GetInt(hndl,'TimestampClockFrequency');
AT_CheckWarning(rc);
fprintf('FrameRate %f fps\nFPGA Clock %d MHz\n',frameRate,clockFreq);

[rc,imagesize] = AT_GetInt(hndl,'ImageSizeBytes');
AT_CheckWarning(rc);
[rc,height] = AT_GetInt(hndl,'AOIHeight');
AT_CheckWarning(rc);
[rc,width] = AT_GetInt(hndl,'AOIWidth');  
AT_CheckWarning(rc);
[rc,stride] = AT_GetInt(hndl,'AOIStride'); 
AT_CheckWarning(rc);
for X = 1:10
    [rc] = AT_QueueBuffer(hndl,imagesize);
    AT_CheckWarning(rc);
end
[rc] = AT_Command(hndl, 'TimestampClockReset');
AT_CheckWarning(rc);
disp('Starting acquisition...');
[rc] = AT_Command(hndl,'AcquisitionStart');
AT_CheckWarning(rc);
buf2 = zeros(width,height);
h=imagesc(buf2);
i=1;
while(i<=frameCount)
    [rc,buf] = AT_WaitBuffer(hndl,1000);
    AT_CheckWarning(rc);
    [rc] = AT_QueueBuffer(hndl,imagesize);
    AT_CheckWarning(rc);
    [rc,buf2] = AT_ConvertMono16ToMatrix(buf,height,width,stride);
    AT_CheckWarning(rc);
    set(h,'CData',buf2);
    drawnow;
    
    %Get timestamp and convert it into seconds
    [rc,ticks] = AT_GetTimeStamp(buf,imagesize);
    time = double(ticks)/double(clockFreq);
    
    AT_CheckWarning(rc);
    fprintf('Frame %d - Ticks %ld, Time %f s\n',i,ticks,time);
    
    i = i+1;
end
disp('Acquisition complete');
[rc] = AT_Command(hndl,'AcquisitionStop');
AT_CheckWarning(rc);
[rc] = AT_Flush(hndl);
AT_CheckWarning(rc);
[rc] = AT_Close(hndl);
AT_CheckWarning(rc);
[rc] = AT_FinaliseLibrary();
AT_CheckWarning(rc);
disp('Camera shutdown');
