disp('Andor SDK3 String Example');
[rc] = AT_InitialiseLibrary();
AT_CheckError(rc);
[rc,hndl] = AT_Open(0);
AT_CheckError(rc);
disp('Camera initialized');
strLen = 256;
[rc, serialNumber] = AT_GetString(hndl,'SerialNumber',strLen)
AT_CheckWarning(rc);
[rc, maxLength] = AT_GetStringMaxLength(hndl,'SerialNumber')
AT_CheckWarning(rc);
[rc, writable] = AT_IsWritable(hndl,'SerialNumber')
AT_CheckWarning(rc);
if writable==1
    [rc] = AT_SetString(hndl,'SerialNumber','NewStringValue')
    AT_CheckWarning(rc);
end
[rc] = AT_Flush(hndl);
AT_CheckWarning(rc);
[rc] = AT_Close(hndl);
AT_CheckWarning(rc);
[rc] = AT_FinaliseLibrary();
AT_CheckWarning(rc);
disp('Camera shutdown');