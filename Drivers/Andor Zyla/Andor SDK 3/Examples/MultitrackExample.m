function varargout = MultitrackExample(varargin)
% MULTITRACKEXAMPLE MATLAB code for MultitrackExample.fig
%      MULTITRACKEXAMPLE, by itself, creates a new MULTITRACKEXAMPLE or raises the existing
%      singleton*.
%
%      H = MULTITRACKEXAMPLE returns the handle to a new MULTITRACKEXAMPLE or the handle to
%      the existing singleton*.
%
%      MULTITRACKEXAMPLE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MULTITRACKEXAMPLE.M with the given input arguments.
%
%      MULTITRACKEXAMPLE('Property','Value',...) creates a new MULTITRACKEXAMPLE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MultitrackExample_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MultitrackExample_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MultitrackExample

% Last Modified by GUIDE v2.5 16-Sep-2015 16:14:57

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @MultitrackExample_OpeningFcn, ...
                   'gui_OutputFcn',  @MultitrackExample_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before MultitrackExample is made visible.
function MultitrackExample_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MultitrackExample (see VARARGIN)

% Choose default command line output for MultitrackExample
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% This sets up the initial plot - only do when we are invisible
% so window can get raised using MultitrackExample.
if strcmp(get(hObject,'Visible'),'off')
    plot(0);
end

% UIWAIT makes MultitrackExample wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = MultitrackExample_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

delete(handles.figure1)


% --- Executes on slider movement.
function mtCurrentTrackSlider_Callback(hObject, eventdata, handles)
% hObject    handle to mtCurrentTrackSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
plotdata();

% --- Plots captured data on the graph
function plotdata()
handles = guidata(gcbo);
global y;
axes(handles.axes1);
cla;

%Update display based on slider position
mtCount = str2num(get(handles.ebCount, 'string'));
value = get(handles.mtCurrentTrackSlider, 'Value');

if (value > mtCount)
    value = mtCount;
    set(handles.mtCurrentTrackSlider, 'Value', mtCount);
elseif(value == 0)
    value = 1;
    set(handles.mtCurrentTrackSlider, 'Value', 1);
end;
plot(y(:,int64(value)));

% --- Executes during object creation, after setting all properties.
function mtCurrentTrackSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mtCurrentTrackSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
max = 100.0;
set(hObject, 'Min', 1.0);
set(hObject, 'Max', max+1);
set(hObject, 'Value', 1.0); 
set(hObject, 'SliderStep', [1.0/max,10.0/max]); 



function ebCount_Callback(hObject, eventdata, handles)
% hObject    handle to ebCount (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ebCount as text
%        str2double(get(hObject,'String')) returns contents of ebCount as a double

global oldmtCount;
mtCount = str2num(get(handles.ebCount, 'string'));
d1 = get(handles.mtTable,'Data'); 
newField = [0, 0];
if ( oldmtCount > 0)
    newField = [d1(1,1), d1(1,2)];
end
d2 = cat(1,newField);
for x = 2:mtCount
    newField = [0, 0];
    if (x <= oldmtCount)
        newField =  [d1(x,1), d1(x,2)];
    end
    disp(newField);
    d2 = cat(1,d2,newField);
end
oldmtCount = mtCount;
set(handles.mtTable,'Data',d2);

% --- Executes during object creation, after setting all properties.
function ebCount_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ebCount (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in pbAcquire.
function pbAcquire_Callback(hObject, eventdata, handles)
% hObject    handle to pbAcquire (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA) 

global hndl;
global y;
global aoiwidth;
%hndl = 102

mtCount= str2num(get(handles.ebCount, 'string'));
%Set Multitrack parameters
[rc] = AT_SetEnumString(hndl,'AOILayout','Multitrack');
AT_CheckWarning(rc);
[rc] = AT_SetInt(hndl,'MultitrackSelector', 0); 
AT_CheckWarning(rc);  
[rc] = AT_SetInt(hndl,'MultitrackCount', mtCount); 
AT_CheckWarning(rc);

d = get(handles.mtTable,'Data'); 
for x = 1:mtCount   
    [rc] = AT_SetInt(hndl,'MultitrackSelector', x-1); 
    AT_CheckWarning(rc);  
    [rc] = AT_SetInt(hndl,'MultitrackStart',d(x,1)); 
    AT_CheckWarning(rc);
    [rc] = AT_SetInt(hndl,'MultitrackEnd', d(x,2)); 
    AT_CheckWarning(rc);
end

%get image settings
[rc,imagesize] = AT_GetInt(hndl,'ImageSizeBytes');
AT_CheckWarning(rc);
[rc,height] = AT_GetInt(hndl,'AOIHeight');
AT_CheckWarning(rc);
[rc,aoiwidth] = AT_GetInt(hndl,'AOIWidth');
[rc,stride] = AT_GetInt(hndl,'AOIStride'); 
AT_CheckWarning(rc);

%Acquire image
set(handles.CamStatusText, 'String', 'Acquiring...');
drawnow;
[rc] = AT_Command(hndl,'AcquisitionStart');
AT_CheckWarning(rc);

%get image data
[rc] = AT_QueueBuffer(hndl,imagesize);
AT_CheckWarning(rc);
[rc] = AT_Command(hndl,'SoftwareTrigger');
AT_CheckWarning(rc);
[rc,buf] = AT_WaitBuffer(hndl,1000);
AT_CheckWarning(rc);
[rc,y] = AT_ConvertMono16ToMatrix(buf,height,aoiwidth,stride);
AT_CheckWarning(rc);

%stop acquisition once finished
[rc] = AT_Command(hndl,'AcquisitionStop');
AT_CheckWarning(rc);
[rc] = AT_Flush(hndl);
AT_CheckWarning(rc);

%set axis display
max = mtCount;
set(handles.mtCurrentTrackSlider, 'Max', max);
if (max == 1)    
    set(handles.mtCurrentTrackSlider, 'SliderStep', [1.0/(max),1.0/(max)]); 
    set(handles.mtCurrentTrackSlider, 'Visible', 'off');
else
    set(handles.mtCurrentTrackSlider, 'SliderStep', [1.0/(max-1),1.0/(max-1)]); 
    set(handles.mtCurrentTrackSlider, 'Visible', 'on');
end
set(handles.mtCurrentTrackSlider, 'Value', 1.0); 
plotdata();
set(handles.CamStatusText, 'String', 'Acquisition complete.');
drawnow;

% --- Executes on button press in pbInitialize.
function pbInitialize_Callback(hObject, eventdata, handles)
% hObject    handle to pbInitialize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global hndl;
global sensorHeight;
%Initialize the camera
set(handles.CamStatusText, 'String', 'Initializing...');
drawnow;
[rc] = AT_InitialiseLibrary();
AT_CheckError(rc);
[rc,hndl] = AT_Open(0);
AT_CheckError(rc);
disp('Camera initialized');
set(handles.CamStatusText, 'String', 'Initialization complete.');
[rc] = AT_SetFloat(hndl,'ExposureTime',0.01);
AT_CheckWarning(rc);
[rc] = AT_SetEnumString(hndl,'SimplePreAmpGainControl','12-bit (low noise)');
AT_CheckWarning(rc);
[rc] = AT_SetEnumString(hndl,'PixelEncoding','Mono16');
AT_CheckWarning(rc);
[rc,sensorHeight] = AT_GetInt(hndl,'SensorHeight');
AT_CheckWarning(rc);
text = strcat('Max =  ', num2str(sensorHeight));
set(handles.mtMax, 'String', text);
ToggleUI(1);

% --- Executes on button press in pbShutdown.
function pbShutdown_Callback(hObject, eventdata, handles)
% hObject    handle to pbShutdown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global hndl;
set(handles.CamStatusText, 'String', 'Shutting down...');
drawnow;
[rc] = AT_Close(hndl);
AT_CheckWarning(rc);
[rc] = AT_FinaliseLibrary();
AT_CheckWarning(rc);
disp('Camera shutdown');
set(handles.CamStatusText, 'String', 'Shutdown complete.');
ToggleUI(0);


% --- Executes during object creation, after setting all properties.
function mtTable_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mtTable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

global oldmtCount;
oldmtCount = 1;
d = [0,0];
set(hObject,'Data', d); 

% --- Function to toggle the UI --- %
function ToggleUI(toggle)
handles = guidata(gcbo);
if (toggle == 1)
    set(handles.pbInitialize, 'Enable', 'off');
    set(handles.pbAcquire, 'Enable', 'on');
    set(handles.pbShutdown, 'Enable', 'on');
    set(handles.ebCount, 'Enable', 'on');
    set(handles.mtTable, 'Enable', 'on');
    set(handles.mtCurrentTrackSlider, 'Enable', 'on');
    set(handles.pbModeFVB, 'Enable', 'on');
    set(handles.pbModeMT, 'Enable', 'off');
else
    set(handles.pbInitialize, 'Enable', 'on');
    set(handles.pbShutdown, 'Enable', 'off');
    set(handles.pbAcquire, 'Enable', 'off');
    set(handles.ebCount, 'Enable', 'off');
    set(handles.mtTable, 'Enable', 'off');
    set(handles.mtCurrentTrackSlider, 'Enable', 'off');
    set(handles.pbModeFVB, 'Enable', 'off');
    set(handles.pbModeMT, 'Enable', 'off');
end


% --- Executes on button press in pbModeMT.
function pbModeMT_Callback(hObject, eventdata, handles)
% hObject    handle to pbModeMT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%disable Multitrack button and enable and enable approproiate options
set(handles.pbModeMT, 'Enable', 'off');
set(handles.pbModeFVB, 'Enable', 'on');
set(handles.ebCount, 'Enable', 'on');
set(handles.mtTable, 'Enable', 'on');


% --- Executes on button press in pbModeFVB.
function pbModeFVB_Callback(hObject, eventdata, handles)
% hObject    handle to pbModeFVB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global sensorHeight;
global oldmtCount;
%disable FVB button and enable and enable approproiate options
oldmtCount = 1;
set(handles.pbModeFVB, 'Enable', 'off');
set(handles.pbModeMT, 'Enable', 'on');
set(handles.ebCount, 'String', '1');
%FVB mode is a single track of full sensor height
d = [1, sensorHeight];
set(handles.mtTable, 'Data', d);
set(handles.ebCount, 'Enable', 'off');
set(handles.mtTable, 'Enable', 'off');
